﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ChemControl.Pages
{
    public partial class UpdateQuantities : System.Web.UI.Page
    {
        List<Pair> standardFilter = new List<Pair>();

        protected void Page_Load(object sender, EventArgs e)
        {
            //add a filter so that only items that need their quantity to be updated are brought back
            standardFilter.Add(new Pair("available", ((int)Availability.Unavailable)));

            quantityRepeater.DataSource = DataManager.GetInventoryUnits(standardFilter);
            quantityRepeater.DataBind();
        }

        protected void submitB_Click(object sender, EventArgs e)
        {
            ProcessUpdate();
        }

        private void ProcessUpdate()
        {
            float newQuantity;
            bool worked = float.TryParse(quantityT.Text, out newQuantity);
            if (newQuantity < 0)
                worked = !worked;
            if (!worked)
            {
                Label2.Text += "Incorrect Quantity : " + quantityT.Text.Trim() + " <br /> ";
            }
            if (worked)
            {
                //Check to see if the item is even checked back in
                standardFilter.Add(new Pair("barcode", barcodeT.Text.Trim()));
                List<InventoryUnit> units = DataManager.GetInventoryUnits(standardFilter);

                //If the barcode turns up a result
                if(units.Count==0)
                {
                    Label2.Text += "Item Does not exist in the database: " + barcodeT.Text.Trim() + " <br /> ";
                }
                if (units.Count > 0)
                {
                    List<Pair> filters = new List<Pair>();
                    filters.Add(new Pair("id", units[0].ID));
                    filters.Add(new Pair("quantity", newQuantity));
                    filters.Add(new Pair("barcode", units[0].Barcode));
                    filters.Add(new Pair("available", ((int)Availability.Available)));
                    filters.Add(new Pair("oldavailable", ((int)Availability.Unavailable)));
                    filters.Add(new Pair("shelf", units[0].Shelf));
                    filters.Add(new Pair("row", units[0].Row));

                    int success = DataManager.EditInventoryUnit(filters);

                    //Display message if successful
                    if (success > 0)
                    {
                        Label2.Text = "Successfully Updated " + units[0].Barcode + " " + units[0].Name + "<br /> ";
                    }
                    else
                    {
                        Label2.Text += "Unsuccessfully Updated " + units[0].Barcode + " " + units[0].Name + "<br /> ";
                    }
                }

            }

        }


    }
}