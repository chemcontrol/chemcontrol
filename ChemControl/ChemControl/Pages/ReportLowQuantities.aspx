﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="ReportLowQuantities.aspx.cs" Inherits="ChemControl.Pages.ReportLowQuantities" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <h1>Low Quantities</h1>
    <asp:ListView ID="reportLV" runat="server">
        <ItemTemplate>
            <div class="reportTile">
                <table>
            <tr>
                <td>ItemName:</td>
                <td>
                    <a href="ItemView.aspx?id=<%# Eval("ID")%>"><%# Eval("Name") %></a>
                </td>
            </tr>
            <tr>
                <td>LowQuantity:</td>
                <td>
                    <%# Eval("LowQuantity") %>
                </td>
            </tr>
            <tr>
                <td>Total:</td>
                <td>
                    <%# Eval("Quantity") %>
                </td>
            </tr>
        </table>
            </div>
        </ItemTemplate>
    </asp:ListView>
    <div>
        <table class="padding">
            <tr>
                <td>
                    <asp:Button ID="Button1" runat="server" Text="Export" OnClick="Button1_Click"/>
                </td>
                <td>
                    <asp:Button ID="Button2" runat="server" Text="Back"
                        OnClientClick="JavaScript:window.history.back(1);return false;"></asp:Button>
                </td>
                <td>
                    <asp:Label ID="errorL" runat="server" Text=""></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>



