﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="UpdateQuantities.aspx.cs" Inherits="ChemControl.Pages.UpdateQuantities" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">

    <h1>Update Quantities</h1>


    <div class="row">
        <div class="inline">
            <asp:TextBox ID="barcodeT" runat="server" Text="Enter Barcode"></asp:TextBox>
        </div>
        <div class="inline">
            <asp:TextBox ID="quantityT" runat="server" Text="Enter Quantity"></asp:TextBox>
        </div>
        <div class="inline">
            <asp:Button ID="submitB" runat="server" Text="Submit" OnClick="submitB_Click" />
        </div>
    </div>

    <div class="row">
        <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
    </div>

    <asp:Repeater ID="quantityRepeater" runat="server">
        <HeaderTemplate>
            <div class="quantityList">
                <table>
                    <tr>
                        <td class="header column">Name
                        </td>
                        <td class="header column">Barcode
                        </td>
                        <td class="header column">Quantity
                        </td>
                    </tr>
        </HeaderTemplate>

        <ItemTemplate>
            <tr>
                <td>
                    <%# Eval("Name") %>
                </td>
                <td>
                    <%# Eval("Barcode") %>
                </td>
                <td>
                    <%# Eval("Quantity") %>
                </td>
            </tr>
        </ItemTemplate>

        <FooterTemplate>
            </table>
              </div>
        </FooterTemplate>
    </asp:Repeater>
    <div class="padding">
        <asp:Button ID="backButton" runat="server" Text="Back"
            OnClientClick="JavaScript:window.history.back(1);return false;"></asp:Button>
    </div>
</asp:Content>
