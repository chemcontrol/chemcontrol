﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" Inherits="Pages_ItemEdit" CodeBehind="ItemView.aspx.cs" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">

    <asp:ListView ID="itemLV" runat="server">

        <ItemTemplate>
            <div>
                <div class="image">
                    <asp:ListView ID="imageLV" OnItemCommand="imageLV_ItemCommand" runat="server">
                        <ItemTemplate>
                            <asp:ImageButton ID="ImageButton1" runat="server" OnClick="ImageButton1_Click" Visible='<%# Eval("IsPrimary") %>' ImageUrl='<%#Eval("FileName") %>' />
                        </ItemTemplate>
                        <EmptyDataTemplate>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl='~/Content/Images/default.png' />
                        </EmptyDataTemplate>
                    </asp:ListView>
                    <asp:FileUpload ID="imageUpload" runat="server" />
                </div>

                <div class="padding">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="Label1" runat="server" Text="Name: "></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="nameT" runat="server" Text='<%# Eval("Name") %>'></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label8" runat="server" Text="CAS #: "></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="TextBox1" runat="server" Text='<%# Eval("CASNumber") %>'></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label2" runat="server" Text="Control ID: "></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="controlIDT" runat="server" Text='<%# Eval("ControlID") %>'></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label3" runat="server" Text="Description: "></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="descriptionT" runat="server" Text='<%# Eval("Description") %>'></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label6" runat="server" Text="Vendor Link: "></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="vendorT" runat="server" Text='<%# Eval("VendorLink") %>'></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label5" runat="server" Text="Re-order Qty: "></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="quantityT" runat="server" Text='<%# Eval("LowQuantity") %>'></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label14" runat="server" Text="Category: "></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="categoriesDDL" runat="server" SelectedValue='<%# Eval("Category") %>'>
                                    <asp:ListItem Value="1">Equipment</asp:ListItem>
                                    <asp:ListItem Value="2">Glassware</asp:ListItem>
                                    <asp:ListItem Value="3">Chemicals</asp:ListItem>
                                    <asp:ListItem Value="4">Hazardous Waste</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label15" runat="server" Text="Unit Type: "></asp:Label>
                            </td>
                            <td>
                                <asp:DropDownList ID="prefixesList" runat="server" SelectedValue='<%# ProcessPrefix(Eval("Unit")) %>'>
                                    <asp:ListItem Value="M">Mega</asp:ListItem>
                                    <asp:ListItem Value="k">Kilo</asp:ListItem>
                                    <asp:ListItem Value="h">Hecto</asp:ListItem>
                                    <asp:ListItem Value="da">Deca</asp:ListItem>
                                    <asp:ListItem Value="">--</asp:ListItem>
                                    <asp:ListItem Value="d">Deci</asp:ListItem>
                                    <asp:ListItem Value="c">Centi</asp:ListItem>
                                    <asp:ListItem Value="m">Milli</asp:ListItem>
                                    <asp:ListItem Value="μ">Micro</asp:ListItem>
                                </asp:DropDownList>
                                <asp:DropDownList ID="suffixesList" runat="server" SelectedValue='<%# ProcessSuffix(Eval("Unit")) %>'>
                                    <asp:ListItem Value="">--</asp:ListItem>
                                    <asp:ListItem Value="Unit">Unit</asp:ListItem>
                                    <asp:ListItem Value="g">Gram</asp:ListItem>
                                    <asp:ListItem Value="L">Liter</asp:ListItem>
                                    <asp:ListItem Value="m">Meter</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </div>
                <asp:Button CommandName="UpdateItemInfo" CommandArgument="UpdateItemInfo" runat="server" Text="Update Item Info" />
                <asp:Label runat="server" Text=""></asp:Label>
            </div>
            <div>
                <h2>Locations</h2>
                <asp:ListView ID="locationLV" runat="server" OnItemCommand="locationLV_ItemCommand" OnItemDataBound="locationLV_ItemDataBound">
                    <ItemTemplate>

                        <p><%# Eval("Name") %></p>
                        <asp:Repeater ID="itemUnits" runat="server">

                            <ItemTemplate>

                                <div class="row">
                                    <div class="inline">
                                        <%#Eval("Barcode") %>
                                    </div>

                                    <div class="inline">
                                        <%#Eval("Quantity") %>
                                    </div>

                                    <div class="inline">
                                        <%#Eval("Shelf") %>
                                    </div>

                                    <div class="inline">
                                        <%#Eval("Row") %>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                        <asp:Label runat="server" Text="Barcode: "></asp:Label>
                        <asp:TextBox ID="barcodeT" runat="server"></asp:TextBox>

                        <asp:Label runat="server" Text="Quantity: "></asp:Label>
                        <asp:TextBox ID="quantityT" runat="server"></asp:TextBox>

                        <asp:Label runat="server" Text="Shelf: "></asp:Label>
                        <asp:TextBox ID="shelfT" runat="server"></asp:TextBox>

                        <asp:Label runat="server" Text="Row: "></asp:Label>
                        <asp:TextBox ID="rowT" runat="server"></asp:TextBox>

                        <asp:Button ID="addUnit" runat="server" CommandArgument="AddUnit" CommandName="AddUnit" Text="Add New Item" />
                    </ItemTemplate>
                </asp:ListView>
            </div>
            <div>
                <p>Safety Info:</p>
                <div class="safety">

                    <div class="nfpa">
                        <asp:ListView ID="nfpaLV" runat="server">
                            <ItemTemplate>
                                <div class="row">
                                    <div class="inline">
                                        <asp:Label ID="Label9" runat="server" Text="Health: "></asp:Label>
                                    </div>
                                    <div class="inline">
                                        <asp:DropDownList ID="healthList" runat="server" SelectedValue='<%#Eval("NFPA_Health") %>'>
                                            <asp:ListItem Value="0">--</asp:ListItem>
                                            <asp:ListItem>1</asp:ListItem>
                                            <asp:ListItem>2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                                            <asp:ListItem>4</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="inline">
                                        <asp:Label ID="Label10" runat="server" Text="Flammability: "></asp:Label>
                                    </div>
                                    <div class="inline">
                                        <asp:DropDownList ID="flammabilityList" runat="server" SelectedValue='<%#Eval("NFPA_Flammability") %>'>
                                            <asp:ListItem Value="0">--</asp:ListItem>
                                            <asp:ListItem>1</asp:ListItem>
                                            <asp:ListItem>2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                                            <asp:ListItem>4</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="inline">
                                        <asp:Label ID="Label11" runat="server" Text="Reactivity: "></asp:Label>
                                    </div>
                                    <div class="inline">
                                        <asp:DropDownList ID="reactivityList" runat="server" SelectedValue='<%#Eval("NFPA_Reactivity") %>'>
                                            <asp:ListItem Value="0">--</asp:ListItem>
                                            <asp:ListItem>1</asp:ListItem>
                                            <asp:ListItem>2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                                            <asp:ListItem>4</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="inline">
                                    <asp:Label ID="Label12" runat="server" Text="Special Hazard: "></asp:Label>
                                </div>
                                <div class="inline">
                                    <asp:DropDownList ID="specialHazardDDL" runat="server" SelectedValue='<%#Eval("NFPA_Special") %>'>
                                        <asp:ListItem Value="">--</asp:ListItem>
                                        <asp:ListItem Value="ALK">Alkaline</asp:ListItem>
                                        <asp:ListItem Value="ACID">Acidic</asp:ListItem>
                                        <asp:ListItem Value="COR">Corrosive</asp:ListItem>
                                        <asp:ListItem Value="OX">Oxidizing</asp:ListItem>
                                        <asp:ListItem Value="ACID">Radioactive</asp:ListItem>
                                        <asp:ListItem Value="W">Reacts Violently with Water</asp:ListItem>
                                        <asp:ListItem Value="WOX">Reacts Violently with Water and Oxygen</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="row">
                                    <div class="inline">
                                        <asp:Label ID="Label13" runat="server" Text="Other Dangers: "></asp:Label>
                                    </div>
                                    <div class="inline">
                                        <asp:TextBox ID="otherDangerT" runat="server" Text='<%#Eval("NFPA_Other") %>'></asp:TextBox>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                    <div class="padding">
                        <asp:ListView ID="ghsLV" runat="server">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <img width="100px" src="../Content/Images/image1.png" />
                                        </td>
                                        <td>
                                            <img width="100px" src="../Content/Images/image2.png" />
                                        </td>
                                        <td>
                                            <img width="100px" src="../Content/Images/image3.png" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox Checked='<%#Eval("GHS_1") %>' ID="ghs1" runat="server" Value="1" Text="Oxidizers"></asp:CheckBox>
                                        </td>
                                        <td>
                                            <asp:CheckBox Checked='<%#Eval("GHS_2") %>' ID="ghs2" runat="server" Value="2" Text="Flammable"></asp:CheckBox>
                                        </td>
                                        <td>
                                            <asp:CheckBox Checked='<%#Eval("GHS_3") %>' ID="ghs3" runat="server" Value="3" Text="Explosive"></asp:CheckBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img width="100px" src="../Content/Images/image4.png" />
                                        </td>
                                        <td>
                                            <img width="100px" src="../Content/Images/image5.png" />
                                        </td>
                                        <td>
                                            <img width="100px" src="../Content/Images/image6.png" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox Checked='<%#Eval("GHS_4") %>' ID="ghs4" runat="server" Value="4" Text="Toxic"></asp:CheckBox>
                                        </td>
                                        <td>
                                            <asp:CheckBox Checked='<%#Eval("GHS_5") %>' ID="ghs5" runat="server" Value="5" Text="Corrosive"></asp:CheckBox>
                                        </td>
                                        <td>
                                            <asp:CheckBox Checked='<%#Eval("GHS_6") %>' ID="ghs6" runat="server" Value="6" Text="Gases Under Pressure"></asp:CheckBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img width="100px" src="../Content/Images/image7.png" />
                                        </td>
                                        <td>
                                            <img width="100px" src="../Content/Images/image8.png" />
                                        </td>
                                        <td>
                                            <img width="100px" src="../Content/Images/image9.png" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:CheckBox Checked='<%#Eval("GHS_7") %>' ID="ghs7" runat="server" Value="7" Text="Health Hazard"></asp:CheckBox>
                                        </td>
                                        <td>
                                            <asp:CheckBox Checked='<%#Eval("GHS_8") %>' ID="ghs8" runat="server" Value="8" Text="Aquatic Toxicity"></asp:CheckBox>
                                        </td>
                                        <td>
                                            <asp:CheckBox Checked='<%#Eval("GHS_9") %>' ID="ghs9" runat="server" Value="9" Text="Irritant"></asp:CheckBox>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <EmptyDataTemplate>
                            </EmptyDataTemplate>
                        </asp:ListView>
                    </div>
                    <div class="sds">
                        <div class="inline">
                            <asp:Label ID="Label7" runat="server" Text="Safety Data Sheet: "></asp:Label>
                        </div>
                        <div class="inline">
                            <%# ProcessSDS(Eval("SDS")) %>
                        </div>
                        <asp:FileUpload ID="sdsUpload" runat="server" />
                    </div>
                    <asp:Button ID="Button1" CommandName="UpdateSafety" CommandArgument="UpdateSafety" runat="server" Text="Update Safety" />
                </div>
            </div>
        </ItemTemplate>
    </asp:ListView>
    <div>
        <asp:Button ID="backButton" runat="server" Text="Back"
            OnClientClick="JavaScript:window.history.back(1);return false;"></asp:Button>
    </div>
</asp:Content>