﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" Inherits="Pages_ItemSearch" CodeBehind="ItemSearch.aspx.cs" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <h1>Inventory</h1>
    <asp:ListView ID="searchLV" runat="server">
        <ItemTemplate>
            <div id="tile">
                <div id="tileImage">
                    <img src='<%# Eval("Image_Path") %>' class="preview" width="100%"/>
                </div>
                <div id="tileTable">
                    <table>
                        <tr>
                            <td>Name: </td>
                            <td>
                                <a href="ItemView.aspx?id=<%# Eval("ID")%>"><%# Eval("Name") %></a>
                            </td>
                        </tr>
                        <tr>
                            <td>CAS #: </td>
                            <td>
                                <%# Eval("CASNumber") %>
                            </td>
                        </tr>
                        <tr>
                            <td>Description: </td>
                            <td>
                                <%# Eval("Description") %>
                            </td>
                        </tr>
                        <tr>
                            <td>Unit: </td>
                            <td>
                                <%# Eval("Unit") %>
                            </td>
                        </tr>
                        <tr>
                            <td>Quantity: </td>
                            <td>
                                <%# Eval("Quantity") %>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </ItemTemplate>

        <EmptyItemTemplate>
            <div class="tile">
                <div class="empty">
                    <p>There are no items matching that search criteria!</p>
                </div>
            </div>
        </EmptyItemTemplate>
        <EmptyDataTemplate>
            <div class="tile">
                <div class="empty">
                    <p>There are no items matching that search criteria!</p>
                </div>
            </div>
        </EmptyDataTemplate>
    </asp:ListView>
    <div>
        <table class="padding">
            <tr>
                <td>
                    <asp:Button ID="Button1" runat="server" Text="Export" OnClick="Button1_Click"/>
                </td>
                <td>
                    <asp:Button ID="Button2" runat="server" Text="Back"
                        OnClientClick="JavaScript:window.history.back(1);return false;"></asp:Button>
                </td>
                <td>
                    <asp:Label ID="errorL" runat="server" Text=""></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
