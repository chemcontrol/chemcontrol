﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ChemControl.Pages
{
    public partial class ItemAdd : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["ItemAddError"] != null)
                errorL.Text = (string)Session["ItemAddError"];

        }

        protected void saveItemClick(object sender, EventArgs e)
        {
            string itemName = itemNameT.Text.Trim();
            bool itemized = itemizedCB.Checked;
            int category = int.Parse(categoriesDDL.SelectedValue);
            string description = descriptionT.Text.Trim();
            string barcode = casNumberT.Text.Trim();
            string controlID = controlNumberT.Text.Trim();
            string quantity = quantityT.Text.Trim();
            string vendorLink = vendorT.Text.Trim();
            string sdsPath = "~/Content/SDS/" + ((sdsUpload.HasFile) ? sdsUpload.FileName : "");
            bool safety = safetyCB.Checked;
            
            string unit = prefixesList.SelectedValue + suffixesList.SelectedValue;
            string health = healthList.SelectedValue;
            string flammability = flammabilityList.SelectedValue;
            string reactivity = reactivityList.SelectedValue;
            string specialHazard = specialHazardDDL.SelectedValue;
            string otherHazard = otherDangerT.Text.Trim();

            Session["ItemAddError"] = "";
            Session["ItemAddError"] += (itemName.Length > 0) ? "" : "Item name must have at least 1 letter!<br />";
            Session["ItemAddError"] += (barcode.Length > 0) ? "" :  casLabel.Text.Substring(0,casLabel.Text.Length - 2) + " must have at least 1 letter!<br />";

            if(category >= 3)
            {
                Session["ItemAddError"] += sdsUpload.HasFile ? "" : "All chemicals and hazardous waste must have an SDS associated!<br />" ;
            }

            Session["ItemAddError"] += (unit.Length > 0) ? "" : "All items must have an associated unit!<br />";
            
            if(Session["ItemAddError"].ToString().Length <= 0)
            {
                List<Pair> filters = new List<Pair>();
                filters.Add(new Pair("name", itemName));
                filters.Add(new Pair("barcode", barcode));

                int sum = DataManager.CheckInventoryItemIsAvailable(filters);

                //Empty sum means that the itemname and barcode are free to use
                if(sum <= 0)
                {
                    filters.Add(new Pair("itemized", itemized));
                    filters.Add(new Pair("category", category));
                    filters.Add(new Pair("controlid", controlID));
                    filters.Add(new Pair("lowquantity", quantity));
                    filters.Add(new Pair("isactive", true));
                    filters.Add(new Pair("description", description));
                    filters.Add(new Pair("vendorlink", vendorLink));
                    filters.Add(new Pair("unit", unit));
                    filters.Add(new Pair("sds", sdsPath));
                    filters.Add(new Pair("safety", safety));

                    int success = DataManager.AddInventoryItem(filters);

                    if (success > 0)
                    {
                        //If a chemical or hazardous waste, then upload the sds.
                        if (category >= 3)
                        {
                            sdsUpload.SaveAs(sdsPath);
                        }


                        filters = new List<Pair>();
                        filters.Add(new Pair("name", itemName));
                        filters.Add(new Pair("barcode", barcode));

                        List<InventoryItem> items = DataManager.GetInventoryItems(filters);

                        int id = items[0].ID;

                        List<Pair> ghsFilters = new List<Pair>();

                        ghsFilters.Add(new Pair("inventoryid", id));
                        ghsFilters.Add(new Pair("ghs1", ghs1.Checked));
                        ghsFilters.Add(new Pair("ghs2", ghs2.Checked));
                        ghsFilters.Add(new Pair("ghs3", ghs3.Checked));
                        ghsFilters.Add(new Pair("ghs4", ghs4.Checked));
                        ghsFilters.Add(new Pair("ghs5", ghs5.Checked));
                        ghsFilters.Add(new Pair("ghs6", ghs6.Checked));
                        ghsFilters.Add(new Pair("ghs7", ghs7.Checked));
                        ghsFilters.Add(new Pair("ghs8", ghs8.Checked));
                        ghsFilters.Add(new Pair("ghs9", ghs9.Checked));
                        success = DataManager.AddGHSItem(ghsFilters);

                        ghsFilters = new List<Pair>();
                        ghsFilters.Add(new Pair("inventoryid", id));

                        List<GHS> ghs = DataManager.GetGHSItem(ghsFilters);

                        List<Pair> nfpaFilters = new List<Pair>();
                        nfpaFilters.Add(new Pair("inventoryid", id));
                        nfpaFilters.Add(new Pair("health", health));
                        nfpaFilters.Add(new Pair("flammability", flammability));
                        nfpaFilters.Add(new Pair("reactivity", reactivity));
                        nfpaFilters.Add(new Pair("special", specialHazard));
                        nfpaFilters.Add(new Pair("other", otherHazard));

                        success = DataManager.AddNFPAItem(nfpaFilters);

                        nfpaFilters = new List<Pair>();
                        nfpaFilters.Add(new Pair("inventoryid", id));

                        List<NFPA> nfpas = DataManager.GetNFPAItems(nfpaFilters);

                        filters = new List<Pair>();
                        filters.Add(new Pair("id", id));
                        filters.Add(new Pair("ghsid", ghs[0].GHS_Id));
                        filters.Add(new Pair("nfpaid", nfpas[0].NFPA_ID));

                        success = DataManager.EditInventoryItem(filters);

                        if (success > 0)
                        {
                            Session["ItemAddError"] = itemName + " was added successfully!";
                            Response.Redirect("ItemAdd.aspx");
                        }
                    }
                    else
                    {
                        Session["ItemAddError"] += "This item has already been added!";
                    }
                }
                else
                {
                    //Print out error if the name/number is not available
                    string label = casLabel.Text.Substring(0, casLabel.Text.Length - 2).ToLower();
                    if(sum >= 3)
                    {
                        Session["ItemAddError"] += "The name and the " + label + " are in use!<br />";
                    } 
                    else if (sum >=2)
                    {
                        Session["ItemAddError"] += "The " + label + " is already in use!<br />";
                    } 
                    else
                    {
                        Session["ItemAddError"] += "The name is already in use!<br />";
                    }

                    errorL.Text = (string)Session["ItemAddError"];
                }

            }

        }

        protected void categoryChange(object sender, EventArgs e)
        {
            switch(int.Parse(categoriesDDL.SelectedValue))
            {
                case 1:
                case 2:
                    casLabel.Text = "Catalog #: ";
                    break;
                case 3:
                case 4:
                    casLabel.Text = "CAS #: ";
                    break;

                default:
                    break;
            }
        }

        protected void safetyChange(object sender, EventArgs e)
        {

        }
    }
}