﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ChemControl.Pages
{
    public partial class UserEditor : System.Web.UI.Page
    {
        List<User> users;
        User user;

        string hide = "hide";
        public string Hide { get { return hide; } set { hide = value; } }

        protected void Page_Load(object sender, EventArgs e)
        {
            users = new List<User>();
            users = DataManager.GetUsers(new List<Pair>());

            //Creates an empty user as a template to add a new one
            User temp = new User() 
            { 
                Username = "Add User",
                Email = "",
                ID = -1,
                IsActive = true,
                IsAdmin = false,
                FirstName = "",
                LastName = "",
                Password = "",
                PasswordExpired = true
            };

            users.Sort();
            users.Insert(0, temp);

            if (!Page.IsPostBack)
                DisplayUser(temp, false);
            else
                DisplayUser(users.Find(x => x.ID == ((int)Session["UserId"])), false);

            
            userRepeat.DataSource = users;
            userRepeat.DataBind();
        }

        protected void User_Click(object sender, EventArgs e)
        {
            string clickedUser = ((LinkButton)sender).Text;

            foreach (User u in users)
            {
                if (clickedUser.Equals(u.Username))
                {
                    //Since clicking the link buttons count as postback
                    //we have to force the user variable to change
                    DisplayUser(u, true);
                    break;
                }
            }
        }

        private void DisplayUser(User change, bool force)
        {
            //So it does not erase the data when you submit
            //Yet it will change the page if you manually select it
            if ( force 
              || !Page.IsPostBack )
            {
                usernameT.Text = change.Username;
                firstT.Text = change.FirstName;
                lastT.Text = change.LastName;
                adminCB.Checked = change.IsAdmin;
                activeCB.Checked = change.IsActive;
                emailT.Text = change.Email;

                Session["UserEditError"] = "";
                errorL.Text = "";
            }

            Hide = (change.ID == -1) ? "hide" : "";
            user = change;
            Session["UserId"] = user.ID;
        }
        protected void saveB_Click(object sender, EventArgs e)
        {
            List<Pair> filters = new List<Pair>();

            filters.Add(new Pair("firstname", firstT.Text.Trim()));
            filters.Add(new Pair("lastname", lastT.Text.Trim()));
            filters.Add(new Pair("username", usernameT.Text.Trim()));
            filters.Add(new Pair("isactive", (activeCB.Checked) ? 1 : 0));
            filters.Add(new Pair("isadmin", (adminCB.Checked) ? 1 : 0));
            filters.Add(new Pair("email", emailT.Text.Trim()));

            //Checks to see if username already exists
            List<Pair> tempFilters = new List<Pair>();
            tempFilters.Add(new Pair("username", usernameT.Text.Trim()));

            List<User> tempUsers = DataManager.GetUsers(tempFilters);

            if (tempUsers.Count > 0)
                if (tempUsers[0].Username.Trim().ToUpper().Equals(user.Username.ToUpper()))
                    tempUsers.RemoveAt(0);

            //Set of criteria for login info
            Session["UserEditError"] = "";
            Session["UserEditError"] += (usernameT.Text.Trim().Length >= 6) ? "" : "Username Must Be at Least 6 Characters!<br />";
            Session["UserEditError"] += (tempUsers.Count <= 0) ? "" : "Username Must Be Unique!<br />";
            Session["UserEditError"] += (emailT.Text.Trim().Length > 0) ? "" : "Must Enter an Email Address!<br />";
            Session["UserEditError"] += (firstT.Text.Trim().Length > 0) ? "" : "Must Enter a First Name!<br />";
            Session["UserEditError"] += (lastT.Text.Trim().Length > 0) ? "" : "Must Enter a Last Name!<br />";

            //Decides if to edit information, or create a new user
            if((int)Session["UserId"] > 0)
            {
                filters.Add(new Pair("id", user.ID));
                filters.Add(new Pair("password", user.Password));
                filters.Add(new Pair("passwordexpired", user.PasswordExpired));

                if(((string)Session["UserEditError"]).Length <= 0)
                {
                    int success = DataManager.EditUser(filters);

                    if (success >= 0)
                    {
                        Session["UserEditError"] = "User information has been successfully changed!";
                        //Refreshes page to see changes
                        Response.Redirect("UserEditor.aspx");
                    }
                    else
                        Session["UserEditError"] = "User information change was unsuccessful!";
                }
                
            }
            else
            {
                
                //Create a random password for account generations
                string password = System.Guid.NewGuid().ToString().Substring(0,8);

                //TODO: Email the user's password to their inbox
                SendPassword(password);

                filters.Add(new Pair("password", password));

                if (((string)Session["UserEditError"]).Length <= 0)
                {
                    int success = DataManager.AddUser(filters);

                    if (success >= 0)
                    {
                        Session["UserEditError"] = "User has been successfully added to the system!";

                        //Refreshes page to see changes
                        Response.Redirect("UserEditor.aspx");
                    }
                    else
                        Session["UserEditError"] = "User could not be added to the system.";
                }
            }

            errorL.Text = (string)Session["UserEditError"];
        }

        private void SendPassword(string pass)
        {

        }

        protected void passwordReset(object sender, EventArgs e)
        {
            string password = System.Guid.NewGuid().ToString().Substring(0, 8);

            SendPassword(password);

            List<Pair> filters = new List<Pair>();

            filters.Add(new Pair("id", user.ID));
            filters.Add(new Pair("firstname", user.FirstName.Trim()));
            filters.Add(new Pair("lastname", user.LastName.Trim()));
            filters.Add(new Pair("username", user.Username.Trim()));
            filters.Add(new Pair("password", password));
            filters.Add(new Pair("isactive", (user.IsActive) ? 1 : 0));
            filters.Add(new Pair("isadmin", (user.IsAdmin) ? 1 : 0));
            filters.Add(new Pair("email", user.Email));
            filters.Add(new Pair("passwordexpired", 1));

            int success = DataManager.EditUser(filters);

            if (success >= 0)
            {
                Session["UserEditError"] = "User information has been successfully changed!";
                //Refreshes page to see changes
                Response.Redirect("UserEditor.aspx");
            }
            else
                Session["UserEditError"] = "User information change was unsuccessful!";
        }
    }
}