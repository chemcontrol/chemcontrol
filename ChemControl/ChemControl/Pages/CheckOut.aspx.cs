﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//change this to not display all available
//change this to not be wrong
// change checkout.aspx to update checkout list on button click like before, check out old program


namespace ChemControl.Pages
{
    public partial class CheckOut : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if(!Page.IsPostBack)
            {
                Session["Items"] = new List<InventoryUnit>();
                Session["ItemLocation"] = DataManager.GetLocations(new List<Pair>());
            }
        }

        // should check availability/exist on button click 
        protected void submitB_Click(object sender, EventArgs e)
        {
            //if (!IsPostBack){
            List<Pair> shop = new List<Pair>();
            shop.Add(new Pair("available", (int)Availability.Available));
            shop.Add(new Pair("barcode", barcodeT.Text.Trim()));
            List<InventoryUnit> item = DataManager.GetInventoryUnits(shop);
            Session["CheckoutErrors"] = "";

            if (item.Count > 0)
            {
                List<InventoryUnit> items = (List<InventoryUnit>)Session["Items"];

                bool unique = true;
                foreach (InventoryUnit i in items)
                {
                    if(i.Barcode == shop[1].Second.ToString())
                    {
                        Session["CheckoutErrors"] += "Barcode is already in use.";
                        unique = false;
                        break;
                    }
                }

                if (unique)
                {
                    items.Add(item[0]);
                    shopListRepeater.DataSource = items;
                    shopListRepeater.DataBind();
                }
            }
            else
                Session["CheckoutErrors"] += "Barcode does not exist or item has already been checked-out.";

            Label2.Text = (string)Session["CheckoutErrors"];
        }
        
        protected void checkO_Click(object sender, EventArgs e)
        {
            ProcessUpdate();
        }
        private void ProcessUpdate()
        {
           
            
            List<InventoryUnit> items = (List<InventoryUnit>)Session["Items"];
            Session["CheckoutErrors"] = "";
            foreach(InventoryUnit i in items)
            {
                List<Pair> filters = new List<Pair>();
                filters.Add(new Pair("id", i.ID));
                filters.Add(new Pair("quantity", i.Quantity));
                filters.Add(new Pair("barcode", i.Barcode));
                filters.Add(new Pair("available", (int)Availability.CheckedOut));
                filters.Add(new Pair("oldavailable", (int)Availability.Available));
                filters.Add(new Pair("shelf", i.Shelf));
                filters.Add(new Pair("row", i.Row));

                int success = DataManager.EditInventoryUnit(filters);

                if (success > 0)
                    Session["CheckoutErrors"] += "Successfully checked Out: " + i.Name + ", " + i.Barcode + "<br />";
                else
                    Session["CheckoutErrors"] += "Unsuccessfully checked Out: " + i.Name + ", " + i.Barcode + "<br />";

                Session["Items"] = new List<InventoryUnit>();
                shopListRepeater.DataSource = new List<InventoryUnit>();
                shopListRepeater.DataBind();
            }

            Label2.Text = (string)Session["CheckoutErrors"];
        }

        protected void shopListRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            if (e.CommandName.Equals("RemoveItem"))
            {
                Control parent = ((Button)e.CommandSource).Parent;
                string barcode = (string)e.CommandArgument;

                List<InventoryUnit> items = (List<InventoryUnit>)Session["Items"];
                for (int i = 0; i < items.Count; i++)
                {
                    if (items[i].Barcode.Equals(barcode))
                    {
                        items.RemoveAt(i);
                        break;
                    }
                }

                Session["Items"] = items;
                shopListRepeater.DataSource = items;
                shopListRepeater.DataBind();
            }
        }

        protected void shopListRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item
                || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                
                DropDownList ddl = (DropDownList)e.Item.FindControl("locationDDL");
                ddl.DataSource = (List<Location>)Session["ItemLocation"];
                ddl.DataBind();
            }
        }
    }
}