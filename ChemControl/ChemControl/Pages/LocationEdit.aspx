﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" Inherits="Pages_LocationEdit" CodeBehind="LocationEdit.aspx.cs" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <div class="padding">
        <h1>Locations</h1>
        <asp:Repeater ID="locationRepeat" runat="server">
            <ItemTemplate>
                <div class="user <%# (Eval("IsActive").ToString().Equals("True")) ? "" : "grey" %>">
                    <asp:LinkButton OnClick="locationClick" runat="server" Text='<%# Eval("Name") %>'></asp:LinkButton>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
    <div class="padding">
        <table>
            <tr>
                <td>Name: 
                </td>
                <td>
                    <asp:TextBox ID="nameT" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>Is Active:
                </td>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="Active Location: "></asp:Label>
                    <asp:CheckBox ID="activeCB" runat="server" />
                </td>
            </tr>
            <tr>
                <td>Description:
                </td>
                <td>
                    <asp:TextBox ID="descriptionT" TextMode="multiline" runat="server">Username</asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="saveB" runat="server" Text="Save" OnClick="saveB_Click" />
                </td>
                <td>
                    <asp:Button ID="backButton" runat="server" Text="Back"
                        OnClientClick="JavaScript:window.history.back(1);return false;"></asp:Button>
                </td>
                <td>
                    <asp:Label ID="errorL" runat="server" Text=""></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
