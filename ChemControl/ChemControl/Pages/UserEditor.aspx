﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" Title="User Editor" Inherits="ChemControl.Pages.UserEditor" CodeBehind="UserEditor.aspx.cs" %>


<asp:Content ContentPlaceHolderID="MainContent" runat="server">

    <div class="users">
        <h1>Users</h1>
        <asp:Repeater ID="userRepeat" runat="server">
            <ItemTemplate>
                <div class="user <%# (Eval("IsActive").ToString().Equals("True")) ? "" : "grey" %>">
                    <asp:LinkButton OnClick="User_Click" runat="server" Text='<%# Eval("Username") %>'></asp:LinkButton>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>

    <div class="userInfo">
        <h2>User Info</h2>
        <div class="padding">
            <table>
                <tr>
                    <td>
                        Username: 
                    </td>
                    <td>
                        <asp:TextBox ID="usernameT" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Is Active:
                    </td>
                    <td>
                        <asp:Label ID="Label3" runat="server" Text="Isactive"></asp:Label>
                    <asp:CheckBox ID="activeCB" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Is Admin: 
                    </td>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="IsAdmin"></asp:Label>
                    <asp:CheckBox ID="adminCB" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        First Name:
                    </td>
                    <td>
                        <asp:TextBox ID="firstT" runat="server">Username</asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Last Name:
                    </td>
                    <td>
                        <asp:TextBox ID="lastT" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        E-mail: 
                    </td>
                    <td>
                        <asp:TextBox ID="emailT" runat="server" TextMode="Email"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:LinkButton runat="server" OnClick="passwordReset" Text="Reset Password"></asp:LinkButton>
                    </td>
                </tr>
            </table>
        </div>
        </div>
        <div class="padding">
            <table>
                <tr>
                    <td>
                        <asp:Button ID="saveB" runat="server" Text="Save" OnClick="saveB_Click" />
                    </td>
                    <td>
                        <asp:Button ID="backButton" runat="server" Text="Back"
                            OnClientClick="JavaScript:window.history.back(1);return false;"></asp:Button>
                    </td>
                    <td>
                        <asp:Label ID="errorL" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
</asp:Content>
