﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master"  Inherits="ChemControl.Pages.CheckIn" Codebehind="CheckIn.aspx.cs" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <h1>Check In</h1>

    <div class="row">
        <div class="inline">
            <asp:TextBox ID="barcodeT" runat="server" Text="Enter Barcode"></asp:TextBox>
        </div>

        <div class="inline">
            <asp:Button ID="submitB" runat="server" Text="Submit" OnClick="submitB_Click" />
        </div>
         <div class="inline">
            <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
        </div>
    </div>

    <div class="quantityList">
        <div class="row">

        </div>
        <asp:Repeater ID="quantityRepeater" runat="server">
            <HeaderTemplate>
              
            </HeaderTemplate>

            <ItemTemplate>
                <div class="row">
                    <div class="name">
                        <%# Eval("Name") %>
                    </div>
                    <div class="barcode">
                        <%# Eval("Barcode") %>
                    </div>
                </div>
            </ItemTemplate>

            <FooterTemplate>
              
            </FooterTemplate>
        </asp:Repeater>
    </div>
    <div class="padding">
        <asp:Button ID="backButton" runat="server" Text="Back"
            OnClientClick="JavaScript:window.history.back(1);return false;"></asp:Button>
    </div>
</asp:Content>
