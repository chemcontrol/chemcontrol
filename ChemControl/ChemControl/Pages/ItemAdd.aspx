﻿<%@ Page Language="C#" Title="Add Inventory Item" MasterPageFile="~/Site.master" AutoEventWireup="true" Inherits="ChemControl.Pages.ItemAdd" CodeBehind="ItemAdd.aspx.cs" %>

<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
    <meta id="Test" />
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">

    <div>
        <h1>Add Item</h1>
        <table class="padding">
            <tr>
                <td>
                    <asp:Label ID="Label8" runat="server" Text="Itemized: "></asp:Label>
                </td>
                <td>
                    <asp:CheckBox ID="itemizedCB" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Text="Name: "></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="itemNameT" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="Description: "></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="descriptionT" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label14" runat="server" Text="Category: "></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="categoriesDDL" OnSelectedIndexChanged="categoryChange" runat="server">
                        <asp:ListItem Value="1">Equipment</asp:ListItem>
                        <asp:ListItem Value="2">Glassware</asp:ListItem>
                        <asp:ListItem Value="3">Chemicals</asp:ListItem>
                        <asp:ListItem Value="4">Hazardous Waste</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="casLabel" runat="server" Text="CAS #: "></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="casNumberT" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="Control #: "></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="controlNumberT" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label4" runat="server" Text="Re-order Qty: "></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="quantityT" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label6" runat="server" Text="Vendor Link: "></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="vendorT" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label5" runat="server" Text="Unit: "></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="prefixesList" runat="server">
                        <asp:ListItem Value="M">Mega</asp:ListItem>
                        <asp:ListItem Value="k">Kilo</asp:ListItem>
                        <asp:ListItem Value="h">Hecto</asp:ListItem>
                        <asp:ListItem Value="da">Deca</asp:ListItem>
                        <asp:ListItem Value="" Selected="True">--</asp:ListItem>
                        <asp:ListItem Value="d">Deci</asp:ListItem>
                        <asp:ListItem Value="c">Centi</asp:ListItem>
                        <asp:ListItem Value="m">Milli</asp:ListItem>
                        <asp:ListItem Value="μ">Micro</asp:ListItem>
                    </asp:DropDownList>
                    <asp:DropDownList ID="suffixesList" runat="server">
                        <asp:ListItem Value="" Selected="True">--</asp:ListItem>
                        <asp:ListItem Value="Unit">Unit</asp:ListItem>
                        <asp:ListItem Value="g">Gram</asp:ListItem>
                        <asp:ListItem Value="L">Liter</asp:ListItem>
                        <asp:ListItem Value="m">Meter</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBox ID="safetyCB" OnCheckedChanged="safetyChange" Text="  Safety Applicable?" runat="server" />
                </td>
            </tr>

            <tr>
                <td>
                    <asp:Label ID="Label7" runat="server" Text="SDS: "></asp:Label>
                </td>
                <td>
                    <asp:FileUpload ID="sdsUpload" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label9" runat="server" Text="Health: "></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="healthList" runat="server">
                        <asp:ListItem Value="0" Selected="True">--</asp:ListItem>
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label10" runat="server" Text="Flammability: "></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="flammabilityList" runat="server">
                        <asp:ListItem Value="0" Selected="True">--</asp:ListItem>
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label11" runat="server" Text="Reactivity: "></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="reactivityList" runat="server">
                        <asp:ListItem Value="0" Selected="True">--</asp:ListItem>
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                        <asp:ListItem>4</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label12" runat="server" Text="Special Hazard: "></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="specialHazardDDL" runat="server">
                        <asp:ListItem Value="" Selected="True">--</asp:ListItem>
                        <asp:ListItem Value="ALK">Alkaline</asp:ListItem>
                        <asp:ListItem Value="ACID">Acidic</asp:ListItem>
                        <asp:ListItem Value="COR">Corrosive</asp:ListItem>
                        <asp:ListItem Value="OX">Oxidizing</asp:ListItem>
                        <asp:ListItem Value="ACID">Radioactive</asp:ListItem>
                        <asp:ListItem Value="W">Reacts Violently with Water</asp:ListItem>
                        <asp:ListItem Value="WOX">Reacts Violently with Water and Oxygen</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label13" runat="server" Text="Other Dangers: "></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="otherDangerT" TextMode="multiline" runat="server"></asp:TextBox>
                </td>
            </tr>
        </table>
    </div>
    <div>
        <table class="padding">
            <tr>
                <td>
                    <img width="100px" src="../Content/Images/image1.png" />
                </td>
                <td>
                    <img width="100px" src="../Content/Images/image2.png" />
                </td>
                <td>
                    <img width="100px" src="../Content/Images/image3.png" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBox ID="ghs1" runat="server" Value="1" Text="Oxidizers"></asp:CheckBox>
                </td>
                <td>
                    <asp:CheckBox ID="ghs2" runat="server" Value="2" Text="Flammable"></asp:CheckBox>
                </td>
                <td>
                    <asp:CheckBox ID="ghs3" runat="server" Value="3" Text="Explosive"></asp:CheckBox>
                </td>
            </tr>
            <tr>
                <td>
                    <img width="100px" src="../Content/Images/image4.png" />
                </td>
                <td>
                    <img width="100px" src="../Content/Images/image5.png" />
                </td>
                <td>
                    <img width="100px" src="../Content/Images/image6.png" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBox ID="ghs4" runat="server" Value="4" Text="Toxic"></asp:CheckBox>
                </td>
                <td>
                    <asp:CheckBox ID="ghs5" runat="server" Value="5" Text="Corrosive"></asp:CheckBox>
                </td>
                <td>
                    <asp:CheckBox ID="ghs6" runat="server" Value="6" Text="Gases Under Pressure"></asp:CheckBox>
                </td>
            </tr>
            <tr>
                <td>
                    <img width="100px" src="../Content/Images/image7.png" />
                </td>
                <td>
                    <img width="100px" src="../Content/Images/image8.png" />
                </td>
                <td>
                    <img width="100px" src="../Content/Images/image9.png" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:CheckBox ID="ghs7" runat="server" Value="7" Text="Health Hazard"></asp:CheckBox>
                </td>
                <td>
                    <asp:CheckBox ID="ghs8" runat="server" Value="8" Text="Aquatic Toxicity"></asp:CheckBox>
                </td>
                <td>
                    <asp:CheckBox ID="ghs9" runat="server" Value="9" Text="Irritant"></asp:CheckBox>
                </td>
            </tr>
        </table>
        <table class="padding">
            <tr>
                <td>
                    <asp:Button ID="Button1" runat="server" Text="Save" OnClick="saveItemClick" />
                </td>
                <td>
                    <asp:Button ID="backButton" runat="server" Text="Back"
                        OnClientClick="JavaScript:window.history.back(1);return false;"></asp:Button>
                </td>
                <td>
                    <asp:Label ID="errorL" runat="server" Text=""></asp:Label>
                </td>
            </tr>
        </table>
    </div>

</asp:Content>
