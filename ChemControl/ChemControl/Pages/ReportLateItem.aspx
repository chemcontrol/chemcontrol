﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="ReportLateItem.aspx.cs" Inherits="ChemControl.Pages.ReportLateItem" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <asp:ListView ID="reportLV" runat="server">
        <LayoutTemplate>
            <div class="reportGrid">
                <div class="reportHeader">
                    <div>
                        <p>Barcode</p>
                    </div>
                    <div>
                        <p>Username</p>
                    </div>
                    <div>
                        <p>TimeStamp</p>
                    </div>
                    <div>
                        <p>InventoryName</p>
                    </div>
                    <div>
                        <p>Quantity</p>
                    </div>
                    <div>
                        <p>Location</p>
                    </div>
                </div>
                <asp:PlaceHolder runat="server" ID="groupPlaceHolder"></asp:PlaceHolder>
            </div>
        </LayoutTemplate>

        <GroupTemplate>
            <div class="reportRow">
                <asp:PlaceHolder runat="server" ID="itemPlaceHolder"></asp:PlaceHolder>
            </div>
        </GroupTemplate>
        <ItemTemplate>
            <div class="reportName">
                <%# Eval("Barcode") %>
            </div>

            <div class="reportUser">
                <%# Eval("Username") %>
            </div>

            <div class="reportTime">
                <%# Eval("TimeStamp") %>
            </div>

            <div class="reportInventory">
                <%# Eval("InventoryName") %>
            </div>

            <div class="reportQuantity">
                <%# Eval("Quantity") %>
            </div>

            <div class="reportLocation">
                <%# Eval("Location") %>
            </div>
        </ItemTemplate>
    </asp:ListView>
    <div class="padding">
        <table class="padding">
            <tr>
                <td>
                    <asp:Button ID="Button1" runat="server" Text="Export" OnClick="ExportButton_Click" />
                </td>
                <td>
                    <asp:Button ID="Button2" runat="server" Text="Back"
                        OnClientClick="JavaScript:window.history.back(1);return false;"></asp:Button>
                </td>
                <td>
                    <asp:Label ID="errorL" runat="server" Text=""></asp:Label>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>




