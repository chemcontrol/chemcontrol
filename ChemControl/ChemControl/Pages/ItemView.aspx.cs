﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_ItemEdit : System.Web.UI.Page
{
    List<List<InventoryUnit>> childLists;
    int index;
    string id;

    public bool IsAdmin
    {
        get
        {
            if (Session["Role"] == null)
                return false;

            if ((int)Session["Role"] > 1)
                return true;
            else
                return false;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["ItemViewError"] != null)
            ((Label)itemLV.Controls[0].FindControl("errorL")).Text = Session["ItemViewError"].ToString();
        
        if(!Page.IsPostBack)
        {
            index = 0;
            id = "1";
            if (Request.QueryString["id"] != null)
                id = Request.QueryString["id"];

            List<Pair> filters3 = new List<Pair>();
            filters3.Add(new Pair("isactive", 1));

            List<Pair> filters2 = new List<Pair>();
            filters2.Add(new Pair("inventoryid", id));

            

            List<Pair> filters = new List<Pair>();
            filters.Add(new Pair("id", id));

            List<InventoryItem> item = DataManager.GetInventoryItems(filters);
            
            List<InventoryImage> images = DataManager.GetInventoryImages(filters2);
            List<InventoryUnit> units = DataManager.GetInventoryUnits(filters2);

            List<Pair> filters4 = new List<Pair>();
            filters4.Add(new Pair("id", item[0].NFPA_Id));

            List<Pair> filters5 = new List<Pair>();
            filters5.Add(new Pair("id", item[0].GHS_Id));

            List<GHS> ghsList = DataManager.GetGHSItem(filters5);
            List<NFPA> nfpaList = DataManager.GetNFPAItems(filters4);

            List<Location> locations = DataManager.GetLocations(filters3);
            locations.Sort();

            childLists = new List<List<InventoryUnit>>();

            for (int i = 0; i < locations.Count; i++)
            {
                childLists.Add(units.Where(ele => ele.LocationID == locations[i].ID).Select(ele => ele).ToList());
            }
            
            itemLV.DataSource = item;
            itemLV.DataBind();

            ListView imageLV = (ListView)this.itemLV.Controls[0].FindControl("imageLV");
            imageLV.DataSource = images;
            imageLV.DataBind();


            ListView locationLV = (ListView)this.itemLV.Controls[0].FindControl("locationLV");
            locationLV.DataSource = locations;
            locationLV.DataBind();

            ListView nfpaLV = (ListView)this.itemLV.Controls[0].FindControl("nfpaLV");
            nfpaLV.DataSource = nfpaList;
            nfpaLV.DataBind();

            ListView ghsLV = (ListView)this.itemLV.Controls[0].FindControl("ghsLV");
            ghsLV.DataSource = ghsList;
            ghsLV.DataBind();
        }
        
    }

    public string ProcessSDS(object sdss)
    {
        string sds = sdss.ToString();
        string tag = "";
        if(sds.EndsWith("/"))
        {
            tag += "<p>There is no SDS associated with this item.</p>";
        }
        else
        {
            tag += "<a href='" + sds + "'>SDS</a>";
        }


        return tag;
    }

    public string ProcessSuffix(object unit)
    {
        string suffix = "";
        switch (unit.ToString().Length)
        {
            case 2:
                suffix += unit.ToString().Substring(1, 1);
                break;
            case 3:
                suffix += unit.ToString().Substring(2, 1);
                break;
            case 4:
                suffix += unit.ToString();
                break;
            default:
                break;
        }
        return suffix;
    }

    public string ProcessPrefix(object unit)
    {
        string prefix = "";
        switch(unit.ToString().Length)
        {
            case 2:
                prefix += unit.ToString().Substring(0, 1);
                break;
            case 3:
                prefix += unit.ToString().Substring(0, 2);
                break;
            default:
                break;
        }
        return prefix;
    }

    protected void locationLV_ItemDataBound(object sender, ListViewItemEventArgs e)
    {
        if (e.Item.ItemType == ListViewItemType.DataItem)
        {
            Repeater childRepeater = (Repeater)e.Item.FindControl("itemUnits");
            childRepeater.DataSource = childLists[index];
            childRepeater.DataBind();

            index++;
        }
    }


    protected void locationLV_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        Control loc = ((Button)e.CommandSource).Parent;
        string barcode = ((TextBox)loc.FindControl("barcodeT")).Text;
        string quantity = ((TextBox)loc.FindControl("quantityT")).Text;
        string shelf = ((TextBox)loc.FindControl("shelfT")).Text;
        string row = ((TextBox)loc.FindControl("rowT")).Text;


    }

    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        Control control = ((ImageButton)sender).Parent;
        if(control.Controls.Count > 1)
        {
            ImageButton button = (ImageButton)control.Controls[0], button2 = null;
            bool next = false;

            for(int i = 1; i < control.Controls.Count; i++)
            {
                if(next)
                {
                    button = (ImageButton)control.Controls[i];
                    break;
                }
                if(control.Controls[i].Visible)
                {
                    button2 = (ImageButton)control.Controls[i];
                    next = true;
                }
            }

            if(next)
            {
                button2.Visible = false;
                button.Visible = true;
            }
            else
            {
                button2 = (ImageButton)control.Controls[1];
                button2.Visible = true;
                button.Visible = false;
            }
        }
    }

    protected void imageLV_ItemCommand(object sender, ListViewCommandEventArgs e)
    {

    }
}