﻿<%@ Page Language="C#" AutoEventWireup="true" Title="Check Out" MasterPageFile="~/Site.Master" CodeBehind="CheckOut.aspx.cs" Inherits="ChemControl.Pages.CheckOut" %>

<asp:Content runat="server" ContentPlaceHolderID="MainContent">
    <h1>Check Out</h1>

    <div class="row">
        <div class="inline">
            <asp:TextBox ID="barcodeT" runat="server" Text="Enter Barcode"></asp:TextBox>
        </div>

        <div class="inline">
            <asp:Button ID="submitB" runat="server" Text="Add" OnClick="submitB_Click" />
        </div>
    </div>

    <div class="row">
        <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
    </div>


    <asp:Repeater ID="shopListRepeater" runat="server" OnItemDataBound="shopListRepeater_ItemDataBound" OnItemCommand="shopListRepeater_ItemCommand">
        <HeaderTemplate>
            <div class="quantityList">
                <table>
                    <tr>
                        <td class="header column">Name
                        </td>
                        <td class="header column">Barcode
                        </td>
                        <td class="header column">Location
                        </td>
                        <td class="header column">Remove?
                        </td>
                    </tr>
        </HeaderTemplate>

        <ItemTemplate>
            <tr>
                <td>
                    <%# Eval("Name") %>
                </td>
                <td>
                    <%# Eval("Barcode") %>
                </td>
                <td>
                    <asp:DropDownList ID="locationDDL" runat="server" DataTextField="Name" DataValueField="ID"></asp:DropDownList>
                </td>
                <td>
                    <asp:Button ID="removeB" CommandName="RemoveItem" CommandArgument='<%# Eval("Barcode") %>' runat="server" Text="Remove" />
                </td>
            </tr>
        </ItemTemplate>

        <FooterTemplate>
            </table>
                </div>
        </FooterTemplate>
    </asp:Repeater>



    <div class="padding">
        <table>
            <tr>
                <td>
                    <asp:Button ID="Button1" runat="server" OnClick="checkO_Click" Text="Checkout" />
                </td>
                <td>
                    <asp:Button ID="backButton" runat="server" Text="Back"
                        OnClientClick="JavaScript:window.history.back(1);return false;"></asp:Button>
                </td>
            </tr>
        </table>
    </div>

</asp:Content>
