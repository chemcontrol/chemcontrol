﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ChemControl.Pages
{
    public partial class ReportLowQuantities : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            List<InventoryItem> items = DataManager.GetLowQuantityReport(new List<Pair>());

            reportLV.DataSource = items;
            reportLV.DataBind();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=FileName.xls");
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            reportLV.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();
        }
    }
}