﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Pages_LocationEdit : System.Web.UI.Page
{
    List<Location> locations;
    Location location;
    protected void Page_Load(object sender, EventArgs e)
    {
        locations = DataManager.GetLocations(new List<Pair>());
        location = new Location()
        {
                Description = "",
                ID = -1,
                IsActive = true,
                Name = "Add Location"
        };
        locations.Sort();
        locations.Insert(0, location);

        if (Session["LocationId"] == null)
            DisplayLocation(location, false);
        else
            DisplayLocation(locations.Find(x => x.ID == (int)Session["LocationId"]), false);

        locationRepeat.DataSource = locations;
        locationRepeat.DataBind();
    }

    protected void saveB_Click(object sender, EventArgs e)
    {
        List<Pair> filters = new List<Pair>();

        filters.Add(new Pair("name", nameT.Text.Trim()));
        filters.Add(new Pair("description", descriptionT.Text.Trim()));
        filters.Add(new Pair("isactive", (activeCB.Checked) ? 1 : 0));

        List<Pair> tempFilters = new List<Pair>();
        tempFilters.Add(new Pair("name",nameT.Text.Trim()));

        List<Location> tempLocs = DataManager.GetLocations(tempFilters);

        if (tempLocs.Count > 0)
            if (tempLocs[0].Name.Trim().ToUpper().Equals(location.Name.ToUpper()))
                tempLocs.RemoveAt(0);

        //Set of criteria for login info
        Session["LocationEditError"] = "";
        Session["LocationEditError"] += (nameT.Text.Trim().Length >= 1) ? "" : "Name Must Be at Least 1 Character!<br />";
        Session["LocationEditError"] += (tempLocs.Count <= 0) ? "" : "Name Must Be Unique!<br />";

        //Edit a prexisting location
        if((int)Session["LocationId"] > 0)
        {
            filters.Add(new Pair("id", location.ID));

            if(Session["LocationEditError"].ToString().Length <= 0)
            {
                int success = DataManager.EditLocation(filters);

                if (success > 0)
                {
                    Session["LocationEditError"] = "Location information has been successfully changed!";
                    //Refreshes page to see changes
                    Response.Redirect("LocationEdit.aspx");
                }
                else
                    Session["LocationEditError"] = "Location information change was unsuccessful!";
            }
        }
        else
        {
            //Add new location to the list
            if (Session["LocationEditError"].ToString().Length <= 0)
            {
                int success = DataManager.AddLocation(filters);

                if (success > 0)
                {
                    Session["LocationEditError"] = "Location information has been successfully changed!";
                    //Refreshes page to see changes
                    Response.Redirect("LocationEdit.aspx");
                }
                else
                    Session["LocationEditError"] = "Location information change was unsuccessful!";
            }

        }
    }

    private void DisplayLocation(Location loc, bool force)
    {
        if (force
        || !Page.IsPostBack)
        {
            nameT.Text = loc.Name;
            activeCB.Checked = loc.IsActive;
            descriptionT.Text = loc.Description;

            Session["LocationEditError"] = "";
            errorL.Text = "";
        }

        location = loc;
        Session["LocationId"] = loc.ID;
    }

    protected void locationClick(object sender, EventArgs e)
    {
        string clickedLocation = ((LinkButton)sender).Text;

        foreach (Location loc in locations)
        {
            if (clickedLocation.Equals(loc.Name))
            {
                //Since clicking the link buttons count as postback
                //we have to force the location variable to change
                DisplayLocation(loc, true);
                break;
            }
        }
    }
}