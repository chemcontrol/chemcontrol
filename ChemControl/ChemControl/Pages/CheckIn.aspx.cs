﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ChemControl.Pages
{
    public partial class CheckIn : System.Web.UI.Page
    {
        List<Pair> standardFilter = new List<Pair>();

        protected void Page_Load(object sender, EventArgs e)
        {
            //add a filter so that only items that need their quantity to be updated are brought back
            standardFilter.Add(new Pair("available", ((int)Availability.CheckedOut)));

            quantityRepeater.DataSource = DataManager.GetInventoryUnits(standardFilter);
            quantityRepeater.DataBind();
        }

        protected void submitB_Click(object sender, EventArgs e)
        {
            ProcessUpdate();
        }

        private void ProcessUpdate()
        {
               //Check to see if the item is checked out
               standardFilter.Add(new Pair("barcode", barcodeT.Text.Trim()));
               List<InventoryUnit> units = DataManager.GetInventoryUnits(standardFilter);

               //If the barcode turns up a result
               if (units.Count==0)
               {
                   Label2.Text = "Unsuccessfully checked in " + barcodeT.Text.Trim();
               }
               if (units.Count > 0)
               {
                   List<Pair> filters = new List<Pair>();
                   filters.Add(new Pair("id", units[0].ID));
                   filters.Add(new Pair("quantity", -1));
                   filters.Add(new Pair("barcode", units[0].Barcode));
                   filters.Add(new Pair("available", ((int)Availability.Unavailable)));
                   filters.Add(new Pair("oldavailable", ((int)Availability.CheckedOut)));
                   filters.Add(new Pair("shelf", units[0].Shelf));
                   filters.Add(new Pair("row", units[0].Row));

                   int success = DataManager.EditInventoryUnit(filters);

                   //Display message if successful
                    if (success > 0)
                    {
                        Label2.Text = "Successfully checked in " + units[0].Barcode + " " + units[0].Name + " Please Reload the page for accuracy" ;
                    }
                    if (success==0)
                    {
                        Label2.Text += "Unsuccessfully checked in " + units[0].Barcode + " " + units[0].Name + " || ";
                    }
               }
        }


    }
}