﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;

public enum Availability
{
    Available = 0,
    CheckedOut = 1,
    Unavailable = 2
}

/// <summary>
/// Summary description for DataManager
/// </summary>
public static class DataManager
{
    //private static string checkUser = "";
    private static string GetAllInventoryUnitsDB = "SELECT        Availability, InventoryId AS InventoryID, LocationId, Shelf, Row, Quantity, Barcode, Id," +
                                                   " (SELECT        Name" +
                                                   " FROM            dbo.Location" +
                                                   " WHERE        (Id = ISNULL(dbo.InventoryQuantity.LocationId, Id))) AS LocationName," +
                                                   " (SELECT        Name" +
                                                   " FROM            dbo.InventoryItem" +
                                                   " WHERE        (Id = ISNULL(dbo.InventoryQuantity.InventoryId, Id))) AS ItemName" +
                                                   " FROM            dbo.InventoryQuantity" +
                                                   " WHERE        (Availability = ISNULL(@available, Availability)) AND (InventoryId = ISNULL(@inventoryid, InventoryId)) AND (Barcode = ISNULL(@barcode,Barcode))";

    private static string SetInventoryUnitsDB = "UPDATE [dbo].[InventoryQuantity] " +
                                                " SET [dbo].[InventoryQuantity].Availability = ISNULL(@available, [dbo].[InventoryQuantity].Availability), " +
                                                " [dbo].[InventoryQuantity].Row = ISNULL(@row, [dbo].[InventoryQuantity].Row)," +
                                                " [dbo].[InventoryQuantity].Shelf = ISNULL(@shelf, [dbo].[InventoryQuantity].Shelf), " +
                                                " [dbo].[InventoryQuantity].Quantity = ISNULL(@quantity, [dbo].[InventoryQuantity].Quantity), " +
                                                " [dbo].[InventoryQuantity].Barcode = ISNULL(@barcode, [dbo].[InventoryQuantity].Barcode) " +
                                                " WHERE ([dbo].[InventoryQuantity].Id = @id) AND ([dbo].[InventoryQuantity].Availability=@oldavailable)";

    private static string GetUsersDB = "SELECT Id, IsActive, FirstName, LastName, IsAdmin, Username, Email, Password, PasswordExpired" +
                                     " FROM dbo.[User] " +
                                     " WHERE (@username IS NULL) OR (Username = @username)";

    private static string EditUserDB = "UPDATE [dbo].[User] " +
                                       " SET Username = @username, Email = @email, FirstName = @firstname, PasswordExpired = @passwordexpired, " +
                                       " LastName = @lastname, IsActive = @isactive, IsAdmin = @isadmin, Password = @password " +
                                       " WHERE Id = @id";

    private static string AddUserDB = "INSERT INTO dbo.[User] (IsActive, IsAdmin, FirstName, LastName, Username, Email, Password) " +
                                      " VALUES (@isactive, @isadmin, @firstname, @lastname, @username, @email, @password)";

    private static string AddLocationDB = "INSERT INTO dbo.[Location] ([Name],[Description],[IsActive])" + 
                                          " VALUES (@name, @description, @isactive)";

    private static string GetLocationsDB = "SELECT [Id],[Name],[Description],[IsActive]" +
                                           " FROM dbo.Location" + 
                                           " WHERE Name = ISNULL(@name, Name) AND IsActive=ISNULL(@isactive,IsActive)";

    private static string EditLocationDB = "UPDATE [dbo].[Location] " +
                                           " SET Name = @name, Description = @description, IsActive = @isactive " +
                                           " WHERE Id = @id";

    private static string GetAllActivityReports = "SELECT dbo.Location.Name AS LocationName, dbo.InventoryItem.Name AS InventoryName," + 
                                                  "     dbo.[Activity Log].Id, dbo.[Activity Log].UserId, dbo.[Activity Log].Activity," +
                                                  "     dbo.[Activity Log].TimeStamp, dbo.[Activity Log].Quantity, dbo.[Activity Log].InventoryId, " +
                                                  "     dbo.[Activity Log].LocationId" +
                                                  " FROM dbo.InventoryItem" +
                                                  " INNER JOIN" +
                                                  "     dbo.Location" + 
                                                  " INNER JOIN" +
                                                  "     dbo.[Activity Log] ON dbo.Location.Id = dbo.[Activity Log].LocationId ON dbo.InventoryItem.Id = dbo.[Activity Log].InventoryId" +
                                                  " CROSS JOIN dbo.InventoryQuantity";

    private static string GetLateActivityReports = "SELECT        [Activity Log].InventoryId, [Activity Log].LocationId, InventoryQuantity.Barcode," +
                                                   " [Activity Log].TimeStamp, [Activity Log].UserId, dbo.[User].Username," +
                                                   " [Activity Log].Quantity, [Activity Log].Id, dbo.Location.Name" +
                                                   " FROM            dbo.[Activity Log] AS [Activity Log] INNER JOIN" + 
                                                   " dbo.InventoryQuantity AS InventoryQuantity ON [Activity Log].InventoryId = InventoryQuantity.Id INNER JOIN" + 
                                                   " dbo.InventoryItem AS InventoryItem ON InventoryQuantity.InventoryId = InventoryItem.Id INNER JOIN" + 
                                                   " dbo.[User] ON InventoryItem.Id = dbo.[User].Id AND [Activity Log].UserId = dbo.[User].Id INNER JOIN" +
                                                   " dbo.Location ON [Activity Log].LocationId = dbo.Location.Id" +
                                                   " WHERE        (LEN(InventoryItem.ControlID) > 0) " +
                                                   "          AND (DATEADD(DAY, 4, [Activity Log].TimeStamp) > GETDATE()) "+
                                                   "          AND (InventoryQuantity.Availability = '1')";

    private static string GetLowQuantityItems = "SELECT        Name, LowQuantity, Id, "+
                                                " ISNULL((SELECT        SUM(Quantity) AS Expr1" +
                                                " FROM            dbo.InventoryQuantity AS InventoryQuantity" +
                                                " WHERE        (InventoryId = dbo.InventoryItem.Id)" +
                                                " GROUP BY InventoryId), 0) AS Total" +
                                                " FROM            dbo.InventoryItem" +
                                                " WHERE        (LowQuantity > ISNULL" +
                                                " ((SELECT        SUM(Quantity) AS QuantityTotal" +
                                                " FROM            dbo.InventoryQuantity AS InventoryQuantity" +
                                                " WHERE        (InventoryId = dbo.InventoryItem.Id)" +
                                                " GROUP BY InventoryId), 0))";

    private static string CheckInventoryItemDB = "SELECT        Name, CASNumber" + 
                                                 " FROM            (SELECT        Name, CASNumber" + 
                                                 " FROM            dbo.InventoryItem" +
                                                 " WHERE        (Name = ISNULL(@name, Name)) OR" + 
                                                 "  (CASNumber = ISNULL(@barcode, CASNumber))" + 
                                                 " UNION ALL" + 
                                                 " SELECT        '' AS Name, Retire_Barcode AS CASNumber" +
                                                 " FROM            dbo.Retire" + 
                                                 " WHERE        (Retire_Barcode = ISNULL(@barcode, Retire_Barcode))) AS derivedtbl_1";

    private static string AddInventoryItemDB = "INSERT INTO [dbo].[InventoryItem] ([Name],[Description],[IsActive],[Unit],[LowQuantity],[ControlID],[VendorLink],[Category]," +
                                               " [Itemized],[CASNumber],[SafetyApplicable],[SDS],[NFPA_Id],[GHS_Id])" +
                                               " VALUES (@name, @description, @isactive, @unit, @lowquantity, @controlid, @vendorlink, @category, @itemized, @barcode," +
                                               "         @safety, @sds, @nfpa, @ghs)";

    private static string GetInventoryItemsDB = "SELECT Id, Name, Description, IsActive, Unit, LowQuantity, ControlID, VendorLink, Category, Itemized, CASNumber, SafetyApplicable, SDS, NFPA_Id, GHS_Id" +
                                                " FROM dbo.InventoryItem WHERE (Id=ISNULL(@id, Id)) AND (Name = ISNULL(@name, Name)) AND (CASNumber = ISNULL(@barcode, CASNumber))";

    private static string GetSearchInventoryItemsDB = "SELECT        Id, Name, Description, IsActive, Unit, LowQuantity, ControlID, VendorLink, Category, Itemized, CASNumber, SafetyApplicable, ISNULL" +
                                                      " ((SELECT        SUM(Quantity) AS Expr1" +
                                                      " FROM            dbo.InventoryQuantity AS InventoryQuantity" +
                                                      " WHERE        (InventoryId = dbo.InventoryItem.Id)" +
                                                      " GROUP BY InventoryId), 0) AS Total, ISNULL" +
                                                      " ((SELECT        Image_FileName" +
                                                      " FROM            dbo.InventoryImage" +
                                                      " WHERE        (InventoryItem_Id = Id) AND (Image_IsPrimary = 1)), '~/Content/Images/default.png') AS Image_Path" +
                                                      " FROM            dbo.InventoryItem";

    private static string AddGHSItemDB = "INSERT INTO dbo.[GHS] (InventoryItem_Id, GHS_1, GHS_2, GHS_3, GHS_4, GHS_5, GHS_6, GHS_7, GHS_8, GHS_9) " +
                                         " VALUES (@inventoryid, @ghs1,@ghs2,@ghs3,@ghs4,@ghs5,@ghs6,@ghs7,@ghs8,@ghs9)";

    private static string GetGHSItemDB = "SELECT GHS_Id, InventoryItem_Id, GHS_1, GHS_2, GHS_3, GHS_4, GHS_5, GHS_6, GHS_7, GHS_8, GHS_9" +
                                         " FROM dbo.GHS" +
                                         " WHERE (GHS_Id = ISNULL(@id, GHS_Id)) AND (InventoryItem_Id = ISNULL(@inventoryid, InventoryItem_Id))";

    private static string AddNFPAItemDB = "INSERT INTO [dbo].[NFPA] ([InventoryItem_Id],[NFPA_Health],[NFPA_Flammability],[NFPA_Reactivity],[NFPA_Special],[NFPA_Other])" +
                                          " VALUES (@inventoryid,@health,@flammability,@reactivity,@special,@other) ";

    private static string GetNFPAItemDB = "SELECT  NFPA_ID, InventoryItem_Id, NFPA_Health, NFPA_Flammability, NFPA_Reactivity, NFPA_Special, NFPA_Other" +
                                          " FROM   dbo.NFPA" +
                                          " WHERE  (NFPA_ID = ISNULL(@id, NFPA_ID)) AND (InventoryItem_Id = ISNULL(@inventoryid, InventoryItem_Id))";

    private static string EditInventoryItemDB = "UPDATE [dbo].[InventoryItem]" +
                                                " SET [Name] = ISNULL(@name,[Name]),[Description] = ISNULL(@description,[Description])," + 
                                                " [IsActive] = ISNULL(@isactive,[IsActive]),[Unit] = ISNULL(@unit,[Unit]),[LowQuantity] = ISNULL(@lowquantity,[LowQuantity])," + 
                                                " [ControlID] = ISNULL(@controlid,[ControlID]),[VendorLink] = ISNULL(@vendorlink,[VendorLink]),[Category] = ISNULL(@category,[Category]), " +
                                                " [Itemized] = ISNULL(@itemized,[Itemized]),[CASNumber] = ISNULL(@barcode,[CASNumber]),[SafetyApplicable] = ISNULL(@safety,[SafetyApplicable])," +
                                                " [SDS] = ISNULL(@sds,[SDS]),[NFPA_Id] = ISNULL(@nfpaid,[NFPA_Id]),[GHS_Id] = ISNULL(@ghsid,[GHS_Id])" +
                                                " WHERE [Id]=@id";

    private static string GetInventoryImagesDB = "SELECT TOP (100) PERCENT Image_Id, InventoryItem_Id, ISNULL(Image_FileName, '~/Content/Images/default.png'), Image_IsPrimary, ISNULL(Image_Caption, '')" +
                                                 " FROM dbo.InventoryImage" +
                                                 " WHERE (InventoryItem_Id = @inventoryid)" +
                                                 " ORDER BY Image_IsPrimary";

    public static string ConnectionString
    {
        get
        {
            ConnectionStringSettingsCollection connectionStringSettings = ConfigurationManager.ConnectionStrings;
            return connectionStringSettings["DefaultConnection"].ConnectionString;
        }
    }

    /// <summary>
    /// Returns a DataSet from a specified SQL Select command. SQL parameters should be entered before calling this.
    /// </summary>
    /// <param name="command">An Sql Select command to be executed</param>
    /// <returns>A datatable that contains all returned data</returns>
    private static DataSet GetRows(SqlCommand command)
    {
        DataSet data = null;

        try
        {

            using (SqlConnection con = new SqlConnection(ConnectionString))
            {
                SqlDataAdapter adapter = new SqlDataAdapter();
                command.Connection = con;

                adapter.SelectCommand = command;

                data = new DataSet();
                adapter.Fill(data);
            }
        }
        catch (Exception ex)
        {

        }

        return data;

    }

    private static int ExecuteQuery(SqlCommand command)
    {
        using (SqlConnection con = new SqlConnection(ConnectionString))
        {
            SqlDataAdapter adapter = new SqlDataAdapter();
            command.Connection = con;

            con.Open();

            return command.ExecuteNonQuery();
        }
    }

    private static SqlCommand PrepareSQLCommand(string command, List<Pair> filters)
    {
        /*Dictionary<string, DataTable> filterTables = new Dictionary<string, DataTable>();

        /*foreach (Pair p in filters)
        {
            string filterType = ((string)p.First);

            if (!filterTables.ContainsKey(filterType))
            {
                DataTable temp2 = new DataTable();
                temp2.Columns.Add(filterType);
                filterTables.Add(filterType, temp2);
            }

            DataTable temp = filterTables[filterType];
            DataRow dr = temp.NewRow();
            dr[filterType] = p.Second;
            temp.Rows.Add(dr);
        }*/

        SqlCommand cmd = new SqlCommand(command);

        cmd.CommandType = CommandType.Text;

        foreach (Pair p in filters)
        {
            string parameterTemp = "@" + p.First;
            cmd.Parameters.AddWithValue(parameterTemp, (p.Second));
        }

        int startPos = 0;
        string[] delimits = {")", ",", "\n", "\t",  };
        while((startPos = cmd.CommandText.IndexOf('@', startPos)) > 0)
        {
            int endPos = cmd.CommandText.IndexOf(" ", startPos);
            int tempPos = -1;
            for (int i = 0; i < delimits.Length; i++)
            {
                tempPos = cmd.CommandText.IndexOf(delimits[i], startPos);
                if((endPos > tempPos
                    && !(tempPos <= -1))
                    || endPos <= -1)
                {
                    endPos = tempPos;
                }
            }

            if(endPos <= -1)
                endPos = cmd.CommandText.Length;

            string param = cmd.CommandText.Substring(startPos, endPos - startPos);

            startPos = endPos;

            if (!cmd.Parameters.Contains(param))
                cmd.Parameters.AddWithValue(param, System.DBNull.Value);

        }        
        /*foreach (KeyValuePair<string, DataTable> sqlFilter in filterTables)
        {
            string parameterTemp = "@" + sqlFilter.Key;
            cmd.Parameters.AddWithValue(parameterTemp, ((string)(sqlFilter.Value.Rows[0])));
        }*/

        return cmd;
    }

    public static List<InventoryImage> GetInventoryImages(List<Pair> filters)
    {
        SqlCommand cmd = PrepareSQLCommand(GetInventoryImagesDB, filters);
        DataSet data = GetRows(cmd);

        List<InventoryImage> images = new List<InventoryImage>();

        foreach (DataRow dr in data.Tables[0].Rows)
        {
            InventoryImage image = new InventoryImage();

            if (dr["Image_Id"] != null)
                image.ID = (int)dr["Image_Id"];

            if (dr["InventoryItem_Id"] != null)
                image.InventoryId = (int)dr["InventoryItem_Id"];

            if (dr["Image_FileName"] != null)
                image.FileName = dr["Image_FileName"].ToString();

            if (dr["Image_IsPrimary"] != null)
                image.IsPrimary = (bool)dr["Image_IsPrimary"];

            if (dr["Image_Caption"] != null)
                image.Caption = dr["Image_Caption"].ToString();

            images.Add(image);
        }

        return images;
    }

    public static int CheckInventoryItemIsAvailable(List<Pair> filters)
    {
        SqlCommand cmd = PrepareSQLCommand(CheckInventoryItemDB, filters);
        DataSet data = GetRows(cmd);

        if (data.Tables.Count > 0)
        {
            int sum = 0;

            foreach(DataRow dr in data.Tables[0].Rows)
            {
                if(dr["Name"] != null
                    && dr["Name"].ToString().Trim().Equals(filters[0].Second))
                {
                    sum += 1;
                }

                if (dr["CASNumber"] != null
                    && dr["CASNumber"].ToString().Trim().Equals(filters[1].Second))
                {
                    sum += 2;
                }
            }

            return sum;
        }
        else
            return 0;
    }

    public static List<InventoryItem> GetSearchInventoryItems(List<Pair> filters)
    {
        SqlCommand cmd = PrepareSQLCommand(GetSearchInventoryItemsDB, filters);
        DataSet data = GetRows(cmd);

        List<InventoryItem> items = new List<InventoryItem>();

        foreach(DataRow dr in data.Tables[0].Rows)
        {
            InventoryItem item = new InventoryItem();

            if (dr["Id"] != null)
                item.ID = (int)dr["Id"];

            if (dr["Name"] != null)
                item.Name = (string)dr["Name"];

            if (dr["LowQuantity"] != null)
                item.LowQuantity = float.Parse(dr["LowQuantity"].ToString());

            if (dr["Description"] != null)
                item.Description = ((string)dr["Description"]);

            if (dr["IsActive"] != null)
                item.IsActive = (bool)dr["IsActive"];

            if (dr["Unit"] != null)
                item.Unit = (string)dr["Unit"];

            if (dr["ControlID"] != null)
                item.ControlID = (string)dr["ControlID"];

            if (dr["VendorLink"] != null)
                item.VendorLink = (string)dr["VendorLink"];

            if (dr["Category"] != null)
                item.Category = (int)dr["Category"];

            if (dr["Itemized"] != null)
                item.Itemized = (bool)dr["Itemized"];

            if (dr["CASNumber"] != null)
                item.CASNumber = (string)dr["CASNumber"];

            if (dr["SafetyApplicable"] != null)
                item.SafetyApplicable = (bool)dr["SafetyApplicable"];

            if (dr["Image_Path"] != null)
                item.Image_Path = dr["Image_Path"].ToString();

            items.Add(item);
        }

        return items;
    }

    public static List<InventoryItem> GetInventoryItems(List<Pair> filters)
    {

        SqlCommand cmd = PrepareSQLCommand(GetInventoryItemsDB, filters);
        DataSet data = GetRows(cmd);

        List<InventoryItem> items = new List<InventoryItem>();

        foreach (DataRow dr in data.Tables[0].Rows)
        {
            InventoryItem item = new InventoryItem();

            if (dr["Id"] != null)
                item.ID = (int)dr["Id"];

            if (dr["Name"] != null)
                item.Name = (string)dr["Name"];

            if (dr["LowQuantity"] != null)
                item.LowQuantity = float.Parse(dr["LowQuantity"].ToString());

            if (dr["Description"] != null)
                item.Description = ((string)dr["Description"]);

            if (dr["IsActive"] != null)
                item.IsActive = (bool)dr["IsActive"];

            if (dr["Unit"] != null)
                item.Unit = (string)dr["Unit"];

            if (dr["ControlID"] != null)
                item.ControlID = (string)dr["ControlID"];

            if (dr["VendorLink"] != null)
                item.VendorLink = (string)dr["VendorLink"];

            if (dr["Category"] != null)
                item.Category = (int)dr["Category"];

            if (dr["Itemized"] != null)
                item.Itemized = (bool)dr["Itemized"];

            if (dr["CASNumber"] != null)
                item.CASNumber = (string)dr["CASNumber"];

            if (dr["SafetyApplicable"] != null)
                item.SafetyApplicable = (bool)dr["SafetyApplicable"];

            if (dr["SDS"] != null)
                item.SDS = (string)dr["SDS"];

            if (dr["NFPA_Id"] != null)
            {
                int temp = 0;
                int.TryParse(dr["NFPA_Id"].ToString(), out temp);
                item.NFPA_Id = temp;
            }

            if (dr["GHS_Id"] != null)
            {
                int temp = 0;
                int.TryParse(dr["GHS_Id"].ToString(), out temp);
                item.GHS_Id = temp;
            }

            items.Add(item);
        }

        return items;
    }

    public static List<InventoryUnit> GetInventoryUnits(List<Pair> filters)
    {
        SqlCommand cmd = PrepareSQLCommand(GetAllInventoryUnitsDB, filters);

        DataSet data = GetRows(cmd);
        
        List<InventoryUnit> units = new List<InventoryUnit>();

        foreach(DataRow dr in data.Tables[0].Rows)
        {
            InventoryUnit newUnit = new InventoryUnit();

            if (dr["Id"] != null)
                newUnit.ID = (int)dr["Id"];

            if (dr["ItemName"] != null)
                newUnit.Name = dr["ItemName"].ToString();

            if (dr["Barcode"] != null)
                newUnit.Barcode = (string)dr["Barcode"];

            if (dr["Quantity"] != null)
                newUnit.Quantity = float.Parse(dr["Quantity"].ToString());

            if (dr["Availability"] != null)
                newUnit.Availability = (Availability)((int)dr["Availability"]);

            if (dr["InventoryId"] != null)
                newUnit.InventoryID = (int)dr["InventoryId"];

            if (dr["LocationName"] != null)
                newUnit.LocationName = (string)dr["LocationName"];

            if (dr["LocationId"] != null)
                newUnit.LocationID = (int)dr["LocationId"];

            if (dr["Shelf"] != null)
                newUnit.Shelf = (string)dr["Shelf"];

            if (dr["Row"] != null)
                newUnit.Row = (string)dr["Row"];

            units.Add(newUnit);
        }

        return units;
    }

    public static List<Location> GetLocations(List<Pair> filters)
    {
        SqlCommand cmd = PrepareSQLCommand(GetLocationsDB, filters);
        DataSet data = GetRows(cmd);

        List<Location> locations = new List<Location>();

        if (data.Tables.Count == 0)
            return locations;

        foreach (DataRow dr in data.Tables[0].Rows)
        {
            Location loc = new Location();

            if (dr["Name"] != null)
                loc.Name = (string)dr["Name"];

            if (dr["Id"] != null)
                loc.ID = (int)dr["Id"];

            if (dr["IsActive"] != null)
                loc.IsActive = (bool)dr["IsActive"];

            if (dr["Description"] != null)
                loc.Description = (string)dr["Description"];

            locations.Add(loc);
        }

        return locations;
    }

    public static int EditLocation(List<Pair> filters)
    {
        SqlCommand sql = PrepareSQLCommand(EditLocationDB, filters);

        return ExecuteQuery(sql);
    }

    public static int AddLocation(List<Pair> filters)
    {
        SqlCommand sql = PrepareSQLCommand(AddLocationDB, filters);

        return ExecuteQuery(sql);
    }

    public static List<InventoryItem> GetLowQuantityReport(List<Pair> filters)
    {
        SqlCommand cmd = PrepareSQLCommand(GetLowQuantityItems, filters);
        DataSet data = GetRows(cmd);

        List<InventoryItem> items = new List<InventoryItem>();

        foreach (DataRow dr in data.Tables[0].Rows)
        {
            InventoryItem item = new InventoryItem();

            if (dr["Name"] != null)
                item.Name = (string)dr["Name"];

            if (dr["LowQuantity"] != null)
                item.LowQuantity = float.Parse(dr["LowQuantity"].ToString());

            if (dr["Id"] != null)
                item.ID = (int)dr["Id"];

            if (dr["Total"] != null)
                item.Quantity = float.Parse(dr["Total"].ToString());

            items.Add(item);
        }

        return items;
    }

    public static List<ActivityReport> GetLateReports(List<Pair> filters)
    {
        SqlCommand cmd = PrepareSQLCommand(GetLateActivityReports, filters);
        DataSet data = GetRows(cmd);
        List<ActivityReport> reports = new List<ActivityReport>();

        foreach (DataRow dr in data.Tables[0].Rows)
        {
            ActivityReport ar = new ActivityReport();

            if (dr["LocationName"] != null)
                ar.Location = (string)dr["LocationName"];

            if (dr["LocationId"] != null)
                ar.LocationID = (int)dr["LocationId"];

            if (dr["InventoryName"] != null)
                ar.InventoryName = (string)dr["InventoryName"];

            if (dr["Id"] != null)
                ar.ID = (int)dr["Id"];

            if (dr["Barcode"] != null)
                ar.Barcode = (string)dr["Barcode"];

            if (dr["UserId"] != null)
                ar.UserId = (int)dr["UserId"];

            if (dr["Username"] != null)
                ar.Username = (string)dr["Username"];

            if (dr["TimeStamp"] != null)
                ar.TimeStamp = (string)dr["TimeStamp"];

            if (dr["Quantity"] != null)
                ar.Quantity = float.Parse(dr["Quantity"].ToString());

            if (dr["InventoryId"] != null)
                ar.InventoryID = (int)dr["InventoryId"];

            reports.Add(ar);
        }

        return reports;
    }

    public static List<GHS> GetGHSItem(List<Pair> filters)
    {
        SqlCommand cmd = PrepareSQLCommand(GetGHSItemDB, filters);
        DataSet data = GetRows(cmd);
        List<GHS> ghsList = new List<GHS>();

        foreach (DataRow dr in data.Tables[0].Rows)
        {
            GHS ghs = new GHS();

            if (dr["GHS_Id"] != null)
                ghs.GHS_Id = (int)dr["GHS_Id"];

            if (dr["InventoryItem_Id"] != null)
                ghs.InventoryItem_Id = (int)dr["InventoryItem_Id"];

            if (dr["GHS_1"] != null)
                ghs.GHS_1 = (bool)dr["GHS_1"];

            if (dr["GHS_2"] != null)
                ghs.GHS_2 = (bool)dr["GHS_2"];

            if (dr["GHS_3"] != null)
                ghs.GHS_3 = (bool)dr["GHS_3"];

            if (dr["GHS_4"] != null)
                ghs.GHS_4 = (bool)dr["GHS_4"];

            if (dr["GHS_5"] != null)
                ghs.GHS_5 = (bool)dr["GHS_5"];

            if (dr["GHS_6"] != null)
                ghs.GHS_6 = (bool)dr["GHS_6"];

            if (dr["GHS_7"] != null)
                ghs.GHS_7 = (bool)dr["GHS_7"];

            if (dr["GHS_8"] != null)
                ghs.GHS_8 = (bool)dr["GHS_8"];

            if (dr["GHS_9"] != null)
                ghs.GHS_9 = (bool)dr["GHS_9"];

            ghsList.Add(ghs);
        }

        return ghsList;
    }

    public static List<ActivityReport> GetActivityReports(List<Pair> filters)
    {
        
        SqlCommand cmd = PrepareSQLCommand(GetAllActivityReports, filters);
        DataSet data = GetRows(cmd);
        List<ActivityReport> reports = new List<ActivityReport>();

        foreach(DataRow dr in data.Tables[0].Rows)
        {
            ActivityReport ar = new ActivityReport();

            if (dr["LocationName"] != null)
                ar.Location = (string)dr["LocationName"];

            if (dr["LocationId"] != null)
                ar.LocationID = (int)dr["LocationId"];

            if (dr["InventoryName"] != null)
                ar.InventoryName = (string)dr["InventoryName"];

            if (dr["Id"] != null)
                ar.ID = (int)dr["Id"];

            if (dr["UserId"] != null)
                ar.UserId = (int)dr["UserId"];

            if (dr["Username"] != null)
                ar.Username = (string)dr["Username"];

            if (dr["TimeStamp"] != null)
                ar.TimeStamp = (string)dr["TimeStamp"];

            if (dr["Quantity"] != null)
                ar.Quantity = float.Parse(dr["Quantity"].ToString());

            if (dr["InventoryId"] != null)
                ar.InventoryID = (int)dr["InventoryId"];

            reports.Add(ar);
        }

        return reports;
    }

    public static List<User> GetUsers(List<Pair> filters)
    {
        SqlCommand cmd = PrepareSQLCommand(GetUsersDB, filters);
        DataSet data = GetRows(cmd);

        List<User> users = new List<User>();

        foreach (DataRow dr in data.Tables[0].Rows)
        {
            User user = new User();

            if (dr["Id"] != null)
                user.ID = (int)dr["Id"];

            if (dr["IsActive"] != null)
                user.IsActive = (((bool)dr["IsActive"])) ? true : false;

            if (dr["IsAdmin"] != null)
                user.IsAdmin = (((bool)dr["IsAdmin"])) ? true : false;

            if (dr["FirstName"] != null)
                user.FirstName = (string)dr["FirstName"];

            if (dr["LastName"] != null)
                user.LastName = (string)dr["LastName"];

            if (dr["Username"] != null)
                user.Username = (string)dr["Username"];

            if (dr["Password"] != null)
                user.Password = (string)dr["Password"];

            if (dr["Email"] != null)
                user.Email = (string)dr["Email"];

            if (dr["PasswordExpired"] != null)
                user.PasswordExpired = (((bool)dr["PasswordExpired"])) ? true : false;

            users.Add(user);
        }

        return users;
    }

    public static int AddInventoryItem(List<Pair> filters)
    {
        SqlCommand cmd = PrepareSQLCommand(AddInventoryItemDB, filters);

        return ExecuteQuery(cmd);
    }

    public static int AddUser(List<Pair> filters)
    {
        SqlCommand cmd = PrepareSQLCommand(AddUserDB, filters);

        return ExecuteQuery(cmd);
    }

    public static int EditInventoryItem(List<Pair> filters)
    {
        SqlCommand cmd = PrepareSQLCommand(EditInventoryItemDB, filters);

        return ExecuteQuery(cmd);
    }

    public static int EditInventoryUnit(List<Pair> filters)
    {
        SqlCommand cmd = PrepareSQLCommand(SetInventoryUnitsDB, filters);

        return ExecuteQuery(cmd);
    }

    public static int EditUser(List<Pair> filters)
    {
        SqlCommand cmd = PrepareSQLCommand(EditUserDB, filters);

        return ExecuteQuery(cmd);
    }


    public static int AddNFPAItem(List<Pair> nfpaFilters)
    {
        SqlCommand cmd = PrepareSQLCommand(AddNFPAItemDB, nfpaFilters);

        return ExecuteQuery(cmd);
    }

    public static int AddGHSItem(List<Pair> ghsFilters)
    {
        SqlCommand cmd = PrepareSQLCommand(AddGHSItemDB, ghsFilters);

        return ExecuteQuery(cmd);
    }

    public static List<NFPA> GetNFPAItems(List<Pair> nfpaFilters)
    {
        SqlCommand sql = PrepareSQLCommand(GetNFPAItemDB, nfpaFilters);
        DataSet data = GetRows(sql);
        List<NFPA> nfpas = new List<NFPA>();

        foreach (DataRow dr in data.Tables[0].Rows)
        {
            NFPA nfpa = new NFPA();

            if (dr["NFPA_ID"] != null)
                nfpa.NFPA_ID = (int)dr["NFPA_ID"];

            if (dr["InventoryItem_Id"] != null)
                nfpa.InventoryItem_Id = (int)dr["InventoryItem_Id"];

            if (dr["NFPA_Health"] != null)
                nfpa.NFPA_Health = (int)dr["NFPA_Health"];

            if (dr["NFPA_Flammability"] != null)
                nfpa.NFPA_Flammability = (int)dr["NFPA_Flammability"];

            if (dr["NFPA_Reactivity"] != null)
                nfpa.NFPA_Reactivity = (int)dr["NFPA_Reactivity"];

            if (dr["NFPA_Special"] != null)
                nfpa.NFPA_Special = dr["NFPA_Special"].ToString();

            if (dr["NFPA_Other"] != null)
                nfpa.NFPA_Other = dr["NFPA_Other"].ToString();

            nfpas.Add(nfpa);
        }

        return nfpas;
    }
}