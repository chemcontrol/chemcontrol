﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// will hold user information 
/// </summary>
public class User : IComparable<User>
{

    public int ID
    {
        get;
        set;
    }

    public bool IsActive
    {
        get;
        set;
    }

    public string FirstName
    {
        get;
        set;
    }

    public string LastName
    {
        get;
        set;
    }

    public bool IsAdmin
    {
        get;
        set;
    }

    public string Password
    {
        get;
        set;
    }

    public string Username
    {
        get;
        set;
    }

    public string Email
    {
        get;
        set;
    }

    public bool PasswordExpired
    {
        get;
        set;
    }

	public User()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public int CompareTo(User user)
    {
        int x = 0, y = 0;

        x += (this.IsAdmin) ? 1 : 0;
        y += (user.IsAdmin) ? 1 : 0;

        x += (this.IsActive) ? 2 : 0;
        y += (user.IsActive) ? 2 : 0;

        int temp = x - y;

        if(temp > 0)
        {
            return -1;
        }
        else if (temp < 0)
        {
            return 1;
        }
        else
        {
            return Username.CompareTo(user.Username);
        }
    }
}