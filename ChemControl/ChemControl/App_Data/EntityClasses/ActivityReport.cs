﻿using System;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ActivityReport
/// </summary>
public class ActivityReport
{
	public ActivityReport()
	{
		
	}

    public int ID { get; set; }
    public string Activity { get; set; }
    public string Username { get; set; }
    public string TimeStamp { get; set; }
    public string Location { get; set; }
    public int LocationID { get; set; }
    public float Quantity { get; set; }
    public int InventoryID { get; set; } 
    public string InventoryName { get; set; }

    public int UserId { get; set; } 

    public string Barcode { get; set; }
}