﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// will store information of each location 
/// </summary>
public class Location : IComparable<Location>
{
    public int ID
    {
        get;
        set;
    }

    public string Name
    {
        get;
        set;
    }

    public string Description
    {
        get;
        set;
    }

    public bool IsActive
    {
        get;
        set;
    }
    
	public Location()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public int CompareTo(Location loc)
    {
        int x = 0, y = 0;

        x += (this.IsActive) ? 2 : 0;
        y += (loc.IsActive) ? 2 : 0;

        int temp = x - y;

        if (temp > 0)
        {
            return -1;
        }
        else if (temp < 0)
        {
            return 1;
        }
        else
        {
            return Name.CompareTo(loc.Name);
        }
    }
}