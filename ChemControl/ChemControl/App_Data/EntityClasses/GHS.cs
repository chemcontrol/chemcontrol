﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


    public class GHS
    {
        public GHS()
        {

        }

        public int GHS_Id { get; set; }
        public int InventoryItem_Id { get; set; }
        public bool GHS_1 { get; set; }
        public bool GHS_2 { get; set; }
        public bool GHS_3 { get; set; }
        public bool GHS_4 { get; set; }
        public bool GHS_5 { get; set; }
        public bool GHS_6 { get; set; }
        public bool GHS_7 { get; set; }
        public bool GHS_8 { get; set; }
        public bool GHS_9 { get; set; }
    }
