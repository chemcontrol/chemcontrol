﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// stores additional information for inventory items (images and captions for images) 
/// </summary>
public class InventoryImage
{
    public int ID
    {
        get;
        set;
    }
    public int InventoryId
    {
        get;
        set;
    }
    public string FileName
    {
        get;
        set;
    }
    public bool IsPrimary
    {
        get;
        set;
    }
    public string Caption
    {
        get;
        set;
    }


	public InventoryImage()
	{
		//
		// TODO: Add constructor logic here
		//
	}
}