﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for InventoryUnit
/// </summary>
public class InventoryUnit
{
    public int ID
    {
        get;
        set;
    }

    public string Name
    {
        get;
        set;
    }

    public float Quantity
    {
        get;
        set;
    }

    public string LocationName
    {
        get;
        set;
    }

    public string Barcode
    {
        get;
        set;
    }

    public string Shelf
    {
        get;
        set;
    }

    public string Row
    {
        get;
        set;
    }

    public Availability Availability
    {
        get;
        set;
    }
    public int InventoryID
    {
        get;
        set;
    }

    public int LocationID 
    { 
        get;
        set;
    }

	public InventoryUnit()
	{
		//
		// TODO: Add constructor logic here
		//
	}





    
}