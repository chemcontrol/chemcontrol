﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Inventory item will hold the main class of item to be searched in the db
/// </summary>
public class InventoryItem
{
    public int ID
    {
        get;
        set;
    }
    public string Description
    {
        get;
        set;
    }
    public bool IsActive
    {
        get;
        set;
    }
    public string Unit
    {
        get;
        set;
    }
    public float LowQuantity
    {
        get;
        set;
    }
    public float ReorderQuantity
    {
        get;
        set;
    }
    public string UID
    {
        get;
        set;
    }
    public string VendorLink
    {
        get;
        set;
    }
    public int Category
    {
        get;
        set;
    }
    public bool Itemized
    {
        get;
        set;
    }
    public string CASNumber
    {
        get;
        set;
    }
    public string CatalogNumber
    {
        get;
        set;
    }
    public bool SafetyApplicable
    {
        get;
        set;
    }
    public string SDS
    {
        get;
        set;
    }

   



	public InventoryItem()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public float Quantity { get; set; }

    public string Name { get; set; }

    public string ControlID { get; set; }

    public int NFPA_Id { get; set; }

    public int GHS_Id { get; set; }

    public string Image_Path { get; set; }

    internal void Fill(System.Data.DataRow dr)
    {
        
    }
}