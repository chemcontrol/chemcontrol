﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for NFPA
/// </summary>
public class NFPA
{
	public NFPA()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public string NFPA_Special { get; set; }

    public string NFPA_Other { get; set; }

    public int NFPA_Reactivity { get; set; }

    public int NFPA_Flammability { get; set; }

    public int NFPA_Health { get; set; }

    public int InventoryItem_Id { get; set; }

    public int NFPA_ID { get; set; }
}