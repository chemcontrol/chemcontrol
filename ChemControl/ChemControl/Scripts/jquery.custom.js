﻿$(document).ready(function () {
    AdjustSubmenu();

});

$(window).load(function () {



});

function ShowMenu(menuName) {
    $("." + menuName).css("display", "block");
};

function HideMenu(menuName) {
    $("." + menuName).css("display", "none");
};

$(window).resize(function () {
    AdjustSubmenu();
});

function AdjustSubmenu()
{
    if ((window.innerWidth * .25) < 250) {
        $(".submenu").css("left", Math.round(window.innerWidth * .25));
    }
    else {
        $(".submenu").css("left", "250");
    }
}