﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="ChemControl._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <h1>Login</h1>
    <div id="login">
        <table>
            <tr>
                <td>
                    <asp:Label ID="usernameL" runat="server" Text="Username: "></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="usernameT" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="passwordL" runat="server" Text="Password: "></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="passwordT" runat="server" TextMode="Password"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="loginB" runat="server" Text="Submit" OnClick="loginB_Click" />
                </td>
                <td>
                    <asp:Label ID="errorL" runat="server" ForeColor="#CC0000" ></asp:Label>
                </td>
            </tr>
        </table>
    </div>

</asp:Content>
