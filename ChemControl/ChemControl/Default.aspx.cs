﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ChemControl
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           // Panel myPanel = (Panel)Master.FindControl("Panel1");
           // myPanel.Visible = false;
        }

        protected void loginB_Click(object sender, EventArgs e)
        {
            string username = usernameT.Text.Trim();
            string password = passwordT.Text.Trim();

            List<Pair> filters = new List<Pair>();
            filters.Add(new Pair("username", username));

            List<User> users = DataManager.GetUsers(filters);

            if(users.Count > 0)
            {
                User user = users[0];

                if(user.Password.Equals(password))
                {
                    Session["FirstName"] = user.FirstName;
                    Session["LastName"] = user.LastName;
                    Session["Role"] = (user.IsAdmin) ? 2 : 1;
                    Session["Username"] = user.Username;
                    Session["ID"] = user.ID;

                    Response.Redirect("Home.aspx");
                }

            }
            else
            {
                errorL.Text = "Invalid Username/Password Combination";
            }
        }
    }
}