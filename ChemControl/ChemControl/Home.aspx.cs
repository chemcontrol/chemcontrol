﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ChemControl
{
    public partial class Contact : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Session["Role"] = 1;
            if(Session["Role"] == null)
            {
                Response.Redirect("Default.aspx");
            }
        }

        protected void Admin_PreRender(object sender, EventArgs e)
        {
            Control tag = (Control)sender;

            tag.Visible = ((int)Session["Role"] == 2) ? true : false;
        }
    }
}