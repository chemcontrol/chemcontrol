﻿<%@ Page Title="Contact" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="ChemControl.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Home Page</h1>
    <div>
        <div class="reportTile">
            <h4>Inventory</h4>
            <table>
                <tr>
                    <td></td>
                    <td>
                        <a href="Pages/ItemSearch.aspx">Search for Item</a>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <a href="Pages/CheckIn.aspx">Check-In</a>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <a href="Pages/CheckOut.aspx">Check-Out</a>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <a href="Pages/ItemAdd.aspx">Add Item</a>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <a href="Pages/UpdateQuantities.aspx">Update Quantities</a>
                    </td>
                </tr>
            </table>
        </div>
        <div class="reportTile">
            <h4>Reports</h4>
            <table>
                <tr>
                    <td></td>
                    <td>
                        <a href="Pages/ReportRecent.aspx">Recent Activity</a>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <a href="Pages/ReportLowQuantities.aspx">Low Quantities</a>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <a href="Pages/ReportLateItem.aspx">Late Items</a>
                    </td>
                </tr>
            </table>
        </div>
        <div class="reportTile">
            <h4>Location</h4>
            <table>
                <tr>
                    <td></td>
                    <td>
                        <a href="Pages/LocationEdit.aspx">Location Info</a>
                    </td>
                </tr>
            </table>
        </div>
        <div class="reportTile">
            <h4>User</h4>
            <table>
                <tr>
                    <td></td>
                    <td>
                        <a href="Pages/UserEditor.aspx">User Info</a>
                    </td>
                </tr>
            </table>
        </div>

    </div>
    <div>
        <asp:Button ID="backButton" runat="server" Text="Back"
            OnClientClick="JavaScript:window.history.back(1);return false;"></asp:Button>
    </div>

</asp:Content>
