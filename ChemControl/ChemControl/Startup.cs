﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ChemControl.Startup))]
namespace ChemControl
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
            
        }
    }
}
