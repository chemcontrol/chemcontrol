﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" %>

<!DOCTYPE html>
<script runat="server">

    protected void loginB_Click(object sender, EventArgs e)
    {
        string username = usernameT.Text.Trim();
        string password = passwordT.Text.Trim();

        List<Pair> filters = new List<Pair>();
        filters.Add(new Pair("username", username));

        List<User> users = DataManager.GetUsers(filters);

        if (users.Count > 0)
        {
            User user = users[0];

            if (user.Password.Equals(password))
            {
                Session["FirstName"] = user.FirstName;
                Session["LastName"] = user.LastName;
                Session["Role"] = (user.IsAdmin) ? 2 : 1;
                Session["Username"] = user.Username;
                Session["ID"] = user.ID;

                Response.Redirect("Home.aspx");
            }

        }
        else
        {
            errorL.Text = "Invalid Username/Password Combination";
        }
    }
</script>




<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../Content/Site.css" rel="stylesheet" />
</head>
<body>
    <form runat="server">
        <div class="sideNav">
            <a href="/Default.aspx">
                <img class="logo" src="/Content/Images/logo.jpg" alt="logo" /></a>
        </div>
        <div class="body-content">
            <h1>Login Page</h1>
    <div id="login">
        <table>
            <tr>
                <td>
                    <asp:Label ID="usernameL" runat="server" Text="Username: "></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="usernameT" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="passwordL" runat="server" Text="Password: "></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="passwordT" runat="server" TextMode="Password"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="loginB" runat="server" Text="Submit" OnClick="loginB_Click" />
                </td>
                <td>
                    <asp:Label ID="errorL" runat="server" ForeColor="#CC0000" ></asp:Label>
                </td>
            </tr>
        </table>
    </div>
            <footer>
                <img class="watermark" src="/Content/Images/CCorange.jpg" width="100%" />
            </footer>
        </div>
    </form>
</body>
</html>
